<?php
/**
 * BowtieFW (http://BowtieFW.com)
 *
 * @link      http://BowtieFW.com
 * @copyright Copyright (c) 2014 Nathan Gardner
 * @license   
 */

namespace BowtieFW;

// Install check
if(!file_exists('config/environment.php') && file_exists('install.php')) { header("Location: install.php"); exit(0); }

require('config/environment.php');
require('config/init.php');

// Start session
session_start();

// Make sure Database connection works
try {
	$objDispatcher = new Dispatcher;
	$objDatabase = new Database(DB_SERVER,DB_PORT,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
} catch(\Exception $e) {
	die($e->getMessage());
}

// Dispatch the request
try {
	
	$objDispatcher->parseUrl($params['_urlrequest']);
	$objDispatcher->dispatch();
	
} catch(\Exception $e) {
	
	$objDispatcher->error500($e->getMessage());
	
}

if(DEBUG === true) {
	if(!empty($objDatabase)) {
		echo '<!-- DEBUG DATA '."\r\n".'Numb DB Queries: '.$objDatabase->getNumbQueries().' -->';
	}
}

?>