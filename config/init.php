<?php

namespace BowtieFW;

// CORE
require_once(DIR.'library/Database.php');
require_once(DIR.'library/Dispatcher.php');
require_once(DIR.'library/Model.php');
require_once(DIR.'library/View.php');
require_once(DIR.'library/Controller.php');
require_once(DIR.'library/Authentication.php');
require_once(DIR.'library/Security.php');
require_once(DIR.'library/Email.php');
require_once(DIR.'library/File.php');
require_once(DIR.'library/Node.php');
require_once(DIR.'library/models/PermissionModel.php');
require_once(DIR.'library/Permission.php');
require_once(DIR.'library/models/SitesettingModel.php');
require_once(DIR.'library/Sitesetting.php');
require_once(DIR.'library/models/LogModel.php');
require_once(DIR.'library/models/UserModel.php');
require_once(DIR.'library/models/LayoutModel.php');
require_once(DIR.'library/models/DeviceModel.php');
require_once(DIR.'library/models/MetaModel.php');
require_once(DIR.'library/models/NodeModel.php');
require_once(DIR.'library/models/RouteModel.php');
require_once(DIR.'library/models/SearchModel.php');
require_once(DIR.'library/models/TemplateModel.php');
require_once(DIR.'library/models/PageModel.php');
require_once(DIR.'library/models/CssModel.php');

// CONSTANTS AND GLOBALS
$params = array_merge($_GET,$_POST); // merge GET and POST together
$_GET = $_POST = array(); // REMOVE $_GET and $_POST - YOU SHOULDNT USE IT
$params['uploads'] = !empty($_FILES)?$_FILES:false; // PUT FILE UPLOADS HERE
$params['_urlrequest'] = !empty($params['_urlrequest'])?$params['_urlrequest']:false; // URL REQUEST

// DEBUG
define('DEBUG',true);

?>