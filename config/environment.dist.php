<?php
/**
 *  This file sets up environment variables such as directory paths
 *  
 *  Example environment file.
 */
namespace BowtieFW;

/**
 * Error Reporting
 */
ini_set('display_errors','On');
error_reporting(E_ALL);

/**
 * System Config
 */
define('INSTALLDIR','');
define('DIR',$_SERVER['DOCUMENT_ROOT'].INSTALLDIR.'/');
define('PRODUCT_NAME','Bowtie Framework');
define('URL',$_SERVER['HTTP_HOST'].INSTALLDIR.'/');

/**
 * Database Config
 */
define('DB_SERVER','localhost');
define('DB_PORT','3306');
define('DB_USERNAME','');
define('DB_PASSWORD','');
define('DB_DATABASE','');

/**
 * Email and SMTP Config
 */
define('EMAIL_FROM','noreply@example.com');
define('EMAIL_FROMNAME','noreply');
define('EMAIL_SMTP_HOST',$_SERVER['HTTP_HOST']);
define('EMAIL_SMTP_PORT','25');
define('EMAIL_SMTP_USERNAME','');
define('EMAIL_SMTP_PASSWORD','');


/**
 * Localization
 */
date_default_timezone_set('America/Chicago');

?>
