<div id="header">
    <div class="container">
	<ul class="utilitynav">
            <?php if($LOGGEDIN) { ?>
            <li><a href="<?=$DIR_INSTALL;?>user/profile">My Account</a></li>
            <li><a href="<?=$DIR_INSTALL;?>user/logout">Logout</a></li>
            <?php } else { ?>
            <li><a href="<?=$DIR_INSTALL;?>user/login">Login</a></li>
            <li><a href="<?=$DIR_INSTALL;?>user/register">Register</a></li>
            <?php } ?>
        </ul>
	<div>
            <div id="logo"><a href="<?=$DIR_INSTALL;?>"><img src="<?=$DIR_INSTALL;?>common/img/logo.png" alt="<?=$PRODUCT_NAME;?>" title="<?=$PRODUCT_NAME;?>"/></a></div>
            <ul class="mainnav">
		<li><a href="<?=$DIR_INSTALL;?>">Home</a></li>
                <li><a href="<?=$DIR_INSTALL;?>page/about">About</a></li>
            </ul>
        </div>
    </div>
</div>