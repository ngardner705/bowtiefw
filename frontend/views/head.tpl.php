<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?=$DIR_INSTALL;?>common/css/reset.min.css">
<link rel="stylesheet" href="<?=$DIR_TEMPLATE;?>css/style.css">
<link rel="shortcut icon" type="image/x-icon" href="<?=$DIR_INSTALL;?>favicon.ico">
<link rel="apple-touch-icon" href="<?=$DIR_INSTALL;?>apple-touch-icon.png"/>
<link rel="apple-touch-icon" sizes="76x76" href="<?=$DIR_INSTALL;?>apple-touch-icon-76x76.png"/>
<link rel="apple-touch-icon" sizes="120x120" href="<?=$DIR_INSTALL;?>apple-touch-icon-120x120.png"/>
<link rel="apple-touch-icon" sizes="152x152" href="<?=$DIR_INSTALL;?>apple-touch-icon-152x152.png"/>