<h1>Recover Account</h1>
<form id="userRecover" method="POST" action="<?=$DIR_INSTALL;?>user/recover">
    <fieldset><legend>Recover</legend>
        <div class="row">
            <div class="col4"><label for="recover_email">Email</label></div>
            <div class="col8"><input type="email" placeholder="Email" name="a" id="recover_email"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="recover_code">Code</label></div>
            <div class="col8"><input type="text" placeholder="Code" name="b" id="recover_code"/></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Recover Account"/></div>
        </div>
    </fieldset>
</form>