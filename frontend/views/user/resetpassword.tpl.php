<h1>Reset Password</h1>
<?php
if($passwordUpdated) { ?>
<p>Your password has been updated! <a href="<?=$DIR_INSTALL;?>user/profile">View Profile</a></p>
<?php } else { ?>
<form id="resetPassword" method="POST" action="<?=$DIR_INSTALL;?>user/resetpassword">
    <input type="hidden" name="newpassword" value="1"/>
    <fieldset><legend>New Password</legend>
        <div class="row">
            <div class="col4"><label for="newpw_password">New Password</label></div>
            <div class="col8"><input type="password" placeholder="New Password" name="password" id="newpw_password"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="newpw_password2">Verify Password</label></div>
            <div class="col8"><input type="password" placeholder="Verify Password" name="password2" id="newpw_password2"/></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Reset Password"/></div>
        </div>
    </fieldset>
</form>
<?php } ?>