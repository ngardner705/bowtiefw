<h1>Register</h1>
<form id="userRegister" method="POST" action="<?=$DIR_INSTALL;?>user/register" enctype="multipart/form-data">
    <input type="hidden" name="registerattempt" value="1"/>
    <fieldset><legend>Information</legend>
        <div class="row">
            <div class="col4"><label for="register_email">Email *</label></div>
            <div class="col8"><input type="email" placeholder="Email" name="email" id="register_email" required/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_password">Password *</label></div>
            <div class="col8"><input type="password" placeholder="Password" name="password" id="register_password" required/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_password2">Verify Password *</label></div>
            <div class="col8"><input type="password" placeholder="Verify Password" name="password2" id="register_password2" required/></div>
        </div>
        <!--<div class="row">
            <div class="col4"><label for="register_firstname">First Name</label></div>
            <div class="col8"><input type="text" placeholder="" name="firstname" id="register_firstname"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_lastname">Last Name</label></div>
            <div class="col8"><input type="text" placeholder="" name="lastname" id="register_lastname"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_company">Company</label></div>
            <div class="col8"><input type="text" placeholder="" name="company" id="register_company"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_title">Title</label></div>
            <div class="col8"><input type="text" placeholder="" name="title" id="register_title"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="register_avatar">Image</label></div>
            <div class="col8"><input type="file" name="avatar" id="register_avatar"/></div>
        </div>-->
        <div class="row">
            <div class="col12"><input type="submit" value="Register"/></div>
        </div>
    </fieldset>
</form>