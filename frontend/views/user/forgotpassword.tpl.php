<h1>Forgot Password</h1>
<form id="userForgotpassword" method="POST" action="<?=$DIR_INSTALL;?>user/forgotpassword">
    <input type="hidden" name="passwordreset" value="1"/>
    <fieldset><legend>Recover</legend>
        <div class="row">
            <div class="col4"><label for="forgotpassword_email">Email</label></div>
            <div class="col8"><input type="email" placeholder="Email" name="email" id="forgotpassword_email"/></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Recover Account"/></div>
        </div>
    </fieldset>
</form>