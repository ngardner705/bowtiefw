<h1>Login</h1>
<form id="userLogin" method="POST" action="<?=$DIR_INSTALL;?>user/login">
    <input type="hidden" name="loginattempt" value="1"/>
    <fieldset><legend>Credientials</legend>
        <div class="row">
            <div class="col4"><label for="login_username">Username</label></div>
            <div class="col8"><input type="text" placeholder="Username" name="username" id="login_username"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="login_password">Password</label></div>
            <div class="col8"><input type="password" placeholder="Password" name="password" id="login_password"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="login_remember">Remember Me</label></div>
            <div class="col8"><input type="checkbox" value="1" name="rememberme" id="login_remember"/></div>
        </div>
        <div class="row">
            <div class="col12">
                <p>Don't have a login? <a href="<?=$DIR_INSTALL;?>user/register">Register</a></p>
                <p>Forgot your password? <a href="<?=$DIR_INSTALL;?>user/forgotpassword">Recover Account</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Login"/></div>
        </div>
    </fieldset>
</form>