<h1>Your Account</h1>
<form id="userRegister" method="POST" action="<?=$DIR_INSTALL;?>user/profile" enctype="multipart/form-data">
    <input type="hidden" name="updateprofile" value="1"/>
    <fieldset><legend>Information</legend>
        <div class="row">
            <div class="col4"><label for="profile_username">Username</label></div>
            <div class="col8"><?=$USERINFO['username'];?></div>
        </div>
        <div class="row">
            <div class="col4"><label for="profile_email">Email</label></div>
            <div class="col8"><input type="text" placeholder="Email" name="email" id="profile_email" value="<?=$USERINFO['email'];?>"/></div>
        </div>
        <div class="row">
            <div class="col4"><label>Joined</label></div>
            <div class="col8"><?=date("F jS Y",strtotime($USERINFO['cdate']));?></div>
        </div>
        <div class="row">
            <div class="col4"><label>Password</label></div>
            <div class="col8"><a href="<?=$DIR_INSTALL;?>user/resetpassword">Change Password</a></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Save"/></div>
        </div>
    </fieldset>
</form>