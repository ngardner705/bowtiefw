// Once document is fully loaded, load in the ajax node instances
$(window).load(function() {
    $('.bowtie-ajaxContainer').each(function(index,element) {
        var nodeiid = $(element).data('nodeiid');
        $.ajax({
            url: baseURL+'page/ajaxnode?nodeiid='+nodeiid,
            success: function(data,textStatus,jqXHR) {
                $(element).html(data);
            },
            error: function(jqXHR,textStatus,error) {
                $(element).html("<p>Error loading NodeIID"+nodeiid+"</p>");
            }
        });
    });
});