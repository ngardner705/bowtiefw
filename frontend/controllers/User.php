<?php
namespace BowtieFW\Frontend;

class User extends \BowtieFW\Controller {
    
    private $userModel;
    public $messages;
    
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        $this->userModel = new \BowtieFW\Models\User;
        $this->auth = \BowtieFW\Authentication::getInstance();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $this->view->assign('content','No index action for User');
        $this->finish();
        
    }
    
    /**
     * Login page
     */
    function actionLogin($params='') {
        
        if(!empty($params['loginattempt'])) {
            
            if(!empty($params['username']) && !empty($params['password'])) {
                
                $rememberMe = !empty($params['rememberme'])?true:false;
                
                $loginSuccess = $this->auth->login($params['username'],$params['password'],$rememberMe);
                
                if($loginSuccess) {
                    
                    $redirect = !empty($params['afterlogin'])?$params['afterlogin']:'/User/Profile';
                    $this->redirect($redirect);
                    
                } else {
                    
                    $this->messages[] = end($this->auth->messages); // get auth's last message
                    
                }
                
            } else {
                
                $this->messages[] = array("type"=>"warning","message"=>"Username and Password are required");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('user/login.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Register page
     */
    function actionRegister($params='') {
        
        if(!empty($params['registerattempt'])) {
            
            if(!empty($params['email']) && !empty($params['password'])) {
                
                // verify passwords match
                if($params['password'] == $params['password2']) {
                    
                    // handle image upload
                    //if(!empty($params['uploads']['avatar']['size'])) {
                    //    $objFile = new \BowtieFW\File;
                    //    $avatarInfo = $objFile->upload($params['uploads']['avatar'],'avatars');
                    //} else {
                    //    $avatarInfo = array('file'=>'common/img/default-user.jpg');
                    //}
                    
                    // register them
                    $registerData = array();
                    $registerData['email'] = $params['email'];
                    $registerData['username'] = $params['email'];
                    $registerData['password'] = $params['password'];
                    //$registerData['firstname'] = !empty($params['firstname'])?$params['firstname']:null;
                    //$registerData['lastname'] = !empty($params['lastname'])?$params['lastname']:null;
                    //$registerData['company'] = !empty($params['company'])?$params['company']:null;
                    //$registerData['title'] = !empty($params['title'])?$params['title']:null;
                    //$registerData['dob'] = !empty($params['dob'])?date("Y-m-d",strtotime($params['dob'])):null;
                    //$registerData['address'] = !empty($params['address'])?$params['address']:null;
                    //$registerData['address2'] = !empty($params['address2'])?$params['address2']:null;
                    //$registerData['city'] = !empty($params['city'])?$params['city']:null;
                    //$registerData['province'] = !empty($params['province'])?$params['province']:null;
                    //$registerData['zip'] = !empty($params['zip'])?$params['zip']:null;
                    //$registerData['phone'] = !empty($params['phone'])?$params['phone']:null;
                    //$registerData['avatar'] = !empty($avatarInfo)?$avatarInfo['file']:null;
                    $registerResult = $this->userModel->register($registerData);
                    
                    if($registerResult === false) {
                        
                        $this->messages[] = array("type"=>"error","message"=>"Unable to register, please try again.");
                        
                    } else if($registerResult === -1) {
                        
                        $this->messages[] = array("type"=>"warning","message"=>"This username already exists. <a href=\"/User/Login\">Login</a>");
                        
                    } else {
                        
                        // user is registed, now log them in
                        $loginData = array();
                        $loginData['loginattempt'] = 1;
                        $loginData['username'] = $registerData['username'];
                        $loginData['password'] = $registerData['password'];
                        $loginData['afterlogin'] = '/User/welcome';
                        return $this->actionLogin($loginData);
                        
                    }
                    
                } else {
                    
                    $this->messages[] = array("type"=>"warning","message"=>"Passwords do not match");
                    
                }
                
            } else {
                
                $this->messages[] = array("type"=>"warning","message"=>"Username and Password are required");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('user/register.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Forgot Password
     */
    function actionForgotpassword($params='') {
        
        if(!empty($params['passwordreset'])) {
            
            if(!empty($params['email'])) {
                
                $recoverCode = $this->userModel->forgottenUser($params['email']);
                
                if($recoverCode !== false) {
                    
                    // send recovery email
                    $objEmail = new \BowtieFW\Email;
                    $objEmail->passwordForgot($params['email'],$recoverCode);
                    $this->messages[] = array("type"=>"success","message"=>"Sent recovery email instructions to ".$params['email']);
                    return $this->actionRecover();
                    
                } else {
                    
                    $this->messages[] = array("type"=>"error","message"=>"Cannot recover this account");
                    
                }
                
            } else {
                
                $this->messages[] = array("type"=>"warning","message"=>"Email is required");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('user/forgotpassword.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Recover account with forgotten password
     * Uses the one time recover login to let them in and change password
     */
    function actionRecover($params='') {
        
        if(!empty($params['a'])&&!empty($params['b'])) {
            
            // log them in this one time, and delete the recover code
            $validTempLogin = $this->userModel->recoverUser($params['a'],$params['b']);
            
            if($validTempLogin) {
                
                // log them in
                $userDetails = $this->userModel->getUserByEmail($params['a']);
                if($userDetails) { $this->auth->setLogin($userDetails['id']); }
                $this->redirect("User/resetpassword");
                
                
            } else {
                
                $this->messages[] = array("type"=>"error","message"=>"Invalid email and/or recover code");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('user/recover.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Password reset
     */
    function actionResetpassword($params='') {
        
        $this->auth->requiresAccount();
        
        $passwordUpdated = false;
        
        if(!empty($params['newpassword'])) {
            
            if(!empty($params['password']) && !empty($params['password2'])) {
                
                if($params['password'] == $params['password2']) {
                    
                    $saveData = array();
                    $saveData['id'] = $this->auth->user_id;
                    $saveData['password'] = $params['password'];
                    $saved = $this->userModel->saveUser($saveData);
                    
                    if($saved) {
                        
                        $passwordUpdated = true;
                        
                    } else {
                        
                        $passwordUpdated = false;
                        
                    }
                    
                } else {
                    
                    $this->messages[] = array("type"=>"error","message"=>"Passwords do not match");
                    
                }
                
            } else {
                
                $this->messages[] = array("type"=>"warning","message"=>"New Password and Verify Password are both required");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('passwordUpdated',$passwordUpdated);
        $this->view->assign('content',$this->view->fetch('user/resetpassword.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Welcome new users after registration
     */
    function actionWelcome($params='') {
        
        $this->auth->requiresAccount();
        
        $this->view->assign('content',$this->view->fetch('user/welcome.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * User profile
     */
    function actionProfile($params='') {
        
        $this->auth->requiresAccount();
        
        if(!empty($params['updateprofile'])) {
            
            // handle image upload
            //if(!empty($params['uploads']['avatar']['size'])) {
            //    $objFile = new \BowtieFW\File;
            //    $avatarInfo = $objFile->upload($params['uploads']['avatar'],'avatars');
            //} else {
            //    $avatarInfo = false;
            //}
            
            // register them
            $saveData = array();
            $saveData['id'] = $this->auth->user_id;
            $saveData['email'] = !empty($params['email'])?$params['email']:null;
            //$saveData['firstname'] = !empty($params['firstname'])?$params['firstname']:null;
            //$saveData['lastname'] = !empty($params['lastname'])?$params['lastname']:null;
            //$saveData['company'] = !empty($params['company'])?$params['company']:null;
            //$saveData['title'] = !empty($params['title'])?$params['title']:null;
            //if(!empty($avatarInfo)) { $saveData['avatar'] = $avatarInfo['file']; }
            $saved = $this->userModel->save($saveData);
            
            if($saved) {
                
                $this->messages[] = array("type"=>"success","message"=>"Profile saved");
                
                // view gets userInfo at init, so reset it to new updated info
                $this->view->assign('USERINFO',$this->userModel->get($this->auth->user_id));
                
                
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Unable to save profile");
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('user/profile.tpl.php'));
        $this->finish();
        
    }
    
    /**
     * Log out the user
     */
    function actionLogout($params='') {
        
        $this->auth->logout();
        $this->messages[] = array("type"=>"success","message"=>"You are now logged out.");
        
        // reset the view?
        $this->setPlace('frontend');
        $this->actionLogin();
        
    }
    
}

?>