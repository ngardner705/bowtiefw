<?php
namespace BowtieFW\Frontend;

class Page extends \BowtieFW\Controller {
    
    function __construct() {
        
        parent::__construct();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $objSettings = \BowtieFW\Sitesetting::getInstance();
        $pageId = !empty($params['page_id'])?intval($params['page_id']):$objSettings->getEntry('content','homepage');
        
        if($pageId) {
            
            $objPage = new \BowtieFW\Models\Page;
            $pageInfo = $objPage->getRelatedData($pageId);
            
            echo $this->view->buildHtmlPage($pageInfo);
            
        }
        
    }
    
    function actionAjaxNode($params='') {
        
        $nodeiid = !empty($params['nodeiid'])?intval($params['nodeiid']):false;
        
        $nodeObj = new \BowtieFW\NodeInstance;
        
        if(!empty($nodeiid)) {
            
            $nodeInfo = $nodeObj->getNodeInstance($nodeiid);
            
            $tagsToRender = $this->view->findBowtieTags($nodeInfo['template']['content']);
            
            $markup = $nodeInfo['template']['content'];
            
            foreach($tagsToRender as $tagToRender) {
                
                if($tagToRender['tag'] == 'nodedata') {
                    $renderedTag = $this->view->renderTag($tagToRender,$nodeInfo);
                } else {
                    $renderedTag = $this->view->renderTag($tagToRender);
                }
                
                $markup = str_replace($tagToRender['_lookup'],$renderedTag,$markup);
                
            }
            
        }
        
        echo $markup;
        
    }
    
}

?>