<?php
namespace BowtieFW\Frontend;

class Tests extends \BowtieFW\Controller {
    
    function __construct() {
        
        parent::__construct();
        
    }
    
    function actionIndex($params='') {
        
        $possibleTests = get_class_methods($this);
        $availableTests = array();
        
        foreach($possibleTests as $possibleTest) {
            if(substr($possibleTest,0,6) == 'action' && $possibleTest != 'actionIndex') {
                $availableTests[] = substr($possibleTest,6);
            }
        }
        
        // build content
        $content = '<ul>';
        foreach($availableTests as $test) { $content .= '<li><a href="/Tests/'.$test.'">'.$test.'</a></li>'; }
        $content .= '</ul>';
        
        $this->view->assign('content',$content);
        $this->finish();
        
    }
    
    function actionLogTest($params='') {
        
        $objLog = new \BowtieFW\Models\Log;
        $recentLogs = $objLog->getRecent();
        
        $content = '<pre>'.print_r($recentLogs,true).'</pre>';
        $this->view->assign('content',$content);
        $this->finish();
        
    }
    
    function actionDatabaseTest($params='') {
        
        $objDB = \BowtieFW\Database::getInstance();
        $result = $objDB->setAttribute(\PDO::ATTR_CASE,\PDO::CASE_NATURAL);
        var_dump($result);
        
    }
    
    function actionEvaltest($params='') {
        
        $testValues = array(-1,1,false,true,0);
        
        foreach($testValues as $testValue) {
            echo '<strong>Testing: '; var_dump($testValue); echo '</strong><br/>';
            if($testValue === false) {
                echo '($testValue === false) = true;<br/>';
            } else {
                echo '($testValue === false) = FALSE;<br/>';
            }
            
            if($testValue == false) {
                echo '($testValue == false) = true;<br/>';
            } else {
                echo '($testValue == false) = FALSE;<br/>';
            }
            
            if($testValue === -1) {
                echo '($testValue === -1) = true;<br/>';
            } else {
                echo '($testValue === -1) = FALSE;<br/>';
            }
            
            if($testValue == -1) {
                echo '($testValue == -1) = true;<br/>';
            } else {
                echo '($testValue == -1) = FALSE;<br/>';
            }
            
            if($testValue === true) {
                echo '($testValue === true) = true;<br/>';
            } else {
                echo '($testValue === true) = FALSE;<br/>';
            }
            
            if($testValue == true) {
                echo '($testValue == true) = true;<br/>';
            } else {
                echo '($testValue == true) = FALSE;<br/>';
            }
            echo '<hr/>';
        }
        
    }
    
    function actionEmailtest($params='') {
        
        $objEmail = new \BowtieFW\Email;
        $to = 'ngardner@brand-agent.com';
        
        $sent = $objEmail->genericEmail($to,'Test email','This is a test email, sent at:'.date("Y-m-d H:i:s"));
        echo 'Sending email to '.$to.'<br/>';
        var_dump($sent);
        
    }
    
    function actionException($params='') {
        
        try {
            
            throw new \Exception('Testing');
            
        } catch(\Exception $e) {
            
            echo $e->getMessage();
            
        }
        
    }
    
}
?>