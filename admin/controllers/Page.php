<?php
// Internal admin pages
namespace BowtieFW\Admin;

class Page extends \BowtieFW\Controller {
    
    function __construct() {
        
        parent::__construct();
        $this->auth = \BowtieFW\Authentication::getInstance();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        if($this->auth->loggedIn()) {
            $this->redirect('admin/page/home');
        } else {
            $this->redirect('admin/login');
        }
        
        return;
        
    }
    
    function actionHome($params='') {
        
        $this->view->assign('content',$this->view->fetch('page/home.tpl.php'));
        $this->finish();
        
    }
    
    function actionError($params='') {
        
        $errorMsg = !empty($params['error'])?$params['error']:'Unknown error';
        $this->view->assign('errorMsg',$errorMsg);
        $this->view->assign('content',$this->view->fetch('page/error.tpl.php'));
        $this->finish();
        
    }
    
}

?>