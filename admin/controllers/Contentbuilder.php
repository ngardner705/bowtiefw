<?php
namespace BowtieFW\Admin;

class Contentbuilder extends \BowtieFW\Controller {
    
    public $messages;
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $objNodeType = new \BowtieFW\Models\NodeType;
        $objNode = new \BowtieFW\Models\Node;
        $nodeTypes = $objNodeType->getAll();
        
        foreach($nodeTypes as &$nodeType) {
            $nodeType['numbNodes'] = count($objNode->find(array('nodeType_id'=>$nodeType['id'])));
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('nodeTypes',$nodeTypes);
        $this->view->assign('content',$this->view->fetch('contentbuilder/nodetypes.tpl.php'));
        $this->finish();
        
    }
    
    function actionNew($params='') { $this->actionEdit($params); }
    function actionEdit($params='') {
        
        if(!empty($params['dosave'])) {
            
            $nodeTypeId = $this->saveNodeType($params);
            $params['id'] = $nodeTypeId;
            
        }
        
        $objNodeType = new \BowtieFW\Models\NodeType;
        $objFieldOptions = new \BowtieFW\Models\NodeFieldOption;
        $objFieldInstances = new \BowtieFW\Models\NodeFieldInstance;
        
        if(!empty($params['id'])) {
            $nodeTypeInfo = $objNodeType->get($params['id']);
            $nodeFields = $objFieldInstances->find(array('nodeType_id'=>$nodeTypeInfo['id']));
        } else {
            $nodeTypeInfo = array(
                'id'=>0,
                'title'=>'',
                'description'=>'',
                'keyName'=>''
            );
            $nodeFields = array();
        }
        
        $fieldOptions = $objFieldOptions->getAll();
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('fieldOptions',$fieldOptions);
        $this->view->assign('nodeFields',$nodeFields);
        $this->view->assign('nodeTypeInfo',$nodeTypeInfo);
        $this->view->assign('content',$this->view->fetch('contentbuilder/edit.tpl.php'));
        $this->finish();
        
    }
    
    private function saveNodeType($saveData) {
        
        $objNodeType = new \BowtieFW\Models\NodeType;
        
        $saved = false;
        $tableCreated = false;
        
        $saveRecord = array(
            'id'=>$saveData['nodetype']['id'],
            'title'=>$saveData['nodetype']['title'],
            'description'=>$saveData['nodetype']['description'],
            'keyName'=>$saveData['nodetype']['keyName']
        );
        
        $saved = $objNodeType->save($saveRecord);
        
        if($saved && $tableCreated) {
            return $saved;
        } else {
            return false;
        }
        
    }
    
    private function saveNodeTypeField($saveData) {
        
        
        
    }
    
    // for ajax only
    function actionAjaxAddField($params='') {
        
        $ajaxResponse = array(
            'result'=>'error',
            'response'=>'Unknown error'
        );
        
        if(!empty($params['newfield']['nodeType_id'])) {
            
            if(empty($params['newfield']['keyName']) || empty($params['newfield']['fieldType']) || empty($params['newfield']['title'])) {
                $ajaxResponse['response'] = 'Field Type, Title, and Key Name are required';
            } else {
                
                $objFieldInstances = new \BowtieFW\Models\NodeFieldInstance;
                $saveData = array(
                    'nodeType_id'=>$params['newfield']['nodeType_id'],
                    'fieldType'=>$params['newfield']['fieldType'],
                    'keyName'=>$params['newfield']['keyName'],
                    'title'=>$params['newfield']['title'],
                    'description'=>$params['newfield']['description'],
                    'required'=>!empty($params['newfield']['required'])?true:false,
                    'default'=>$params['newfield']['default'],
                    'options'=>$params['newfield']['options'],
                    'numbAllowedValues'=>$params['newfield']['numbAllowedValues']
                );
                
                $fieldSaved = $objFieldInstances->save($saveData);
                
                if($fieldSaved) {
                    $saveData['id'] = $fieldSaved;
                    $ajaxResponse['result'] = 'success';
                    $ajaxResponse['response'] = $saveData;
                }
                
            }
            
        } else {
            
            $ajaxResponse['response'] = 'Unknown Node Type - save node type before adding fields.';
            
        }
        
        $this->JSONoutput($ajaxResponse);
        
    }
    
}

?>