<?php
namespace BowtieFW\Admin;

class Log extends \BowtieFW\Controller {
    
    public $messages;
    private $logModel;
    
    function __construct() {
        
        parent::__construct();
        $this->logModel = new \BowtieFW\Models\Log;
        $this->messages = array();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $recentLogs = $this->logModel->getRecent(100);
        $userModel = new \BowtieFW\Models\User;
        
        // attach users details
        if(!empty($recentLogs)) {
            foreach($recentLogs as &$aLog) {
                $aLog['user'] = $userModel->get($aLog['user_id']);
            }
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('recentLogs',$recentLogs);
        $this->view->assign('content',$this->view->fetch('log/log.tpl.php'));
        $this->finish();
        
    }
    
}

?>