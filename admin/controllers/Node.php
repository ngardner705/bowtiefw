<?php
namespace BowtieFW\Admin;

class Node extends \BowtieFW\Controller {
    
    public $messages;
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        
    }
    
    // default action
    function actionNodes($params='') {
        
        $objNodes = new \BowtieFW\Models\Node;
        $objNodeTypes = new \BowtieFW\Models\NodeType;
        
        // what nodeType?
        $nodeTypeKey = !empty($params['_extra'][0])?$params['_extra'][0]:false;
        $nodeTypeInfo = $objNodeTypes->getFromKeyname($nodeTypeKey);
        
        $findFilters = array();
        if($nodeTypeInfo['id']) { $findFilters['nodeType_id'] = $nodeTypeInfo['id']; }
        
        $nodeList = $objNodes->find($findFilters);
        
        foreach($nodeList as &$aNode) {
            $aNode = $objNodes->getRelatedData($aNode['id']);
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('nodeType',$nodeTypeInfo);
        $this->view->assign('nodeList',$nodeList);
        $this->view->assign('content',$this->view->fetch('nodes/nodes.tpl.php'));
        $this->finish();
        
    }
    
    function actionEdit($params='') {
        
        $objNode = new \BowtieFW\Node; // this is not the model, its the library
        $objNodeTypes = new \BowtieFW\Models\NodeType;
        $objNodeTypeFields = new \BowtieFW\Models\NodeFieldInstance;
        
        if(!empty($params['dosave'])) {
            
            $nodeId = $this->saveNode($params);
            $params['id'] = $nodeId;
            if($nodeId) {
                $this->messages[] = array("type"=>"success","message"=>"Node saved");
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Unable to save node");
            }
            
        }
        
        // use popup layout if needed
        if(isset($params['_popup'])) { $this->setLayout('popup.tpl.php'); }
        
        // editing or creating?
        if(!empty($params['id'])) {
            // editing, get nodeType based on node
            $nodeInfo = $objNode->getNode($params['id']);
        } else if(!empty($params['nodeType'])) {
            // creating new node, get nodeType based on param
            $nodeInfo = $objNode->newNode($params['nodeType']);
        } else {
            $this->messages[] = array("type"=>"error","message"=>"Unknown node and node type");
            $this->actionNodes();
            return;
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('nodeInfo',$nodeInfo);
        $this->view->assign('content',$this->view->fetch('nodes/edit.tpl.php'));
        $this->finish();
        
    }
    
    private function saveNode($nodeData) {
        
        $objNodes = new \BowtieFW\Models\Node;
        $objNodeTypes = new \BowtieFW\Models\NodeType;
        
        $nodeTypeInfo = $objNodeTypes->get($nodeData['node']['nodeType_id']);
        
        $saveData = array(
            'id'=>!empty($nodeData['node']['id'])?$nodeData['node']['id']:false,
            'title'=>!empty($nodeData['node']['title'])?$nodeData['node']['title']:'New Node',
            'description'=>!empty($nodeData['node']['description'])?$nodeData['node']['description']:'',
            'status'=>!empty($nodeData['node']['status'])?$nodeData['node']['status']:'draft',
            'nodeType_id'=>!empty($nodeData['node']['nodeType_id'])?$nodeData['node']['nodeType_id']:''
        );
        
        $nodeId = $objNodes->save($saveData);
        
        if($nodeId && $nodeData['node']['nodeType_id'] && !empty($nodeData['nodedata'])) {
            
            foreach($nodeData['nodedata'] as $nodeDataField) {
                
                $saveData = array(
                    'node_id'=>$nodeId,
                    'nodeFieldInstance_id'=>$nodeDataField['nodeFieldInstance_id']
                );
                
                if(!empty($nodeDataField['values'])) {
                    
                    foreach($nodeDataField['values'] as $fieldValue) {
                        
                        if(!empty($fieldValue['id']) || !empty($fieldValue['value'])) {
                            
                            $saveData['id'] = $fieldValue['id'];
                            $saveData['value'] = !empty($fieldValue['value'])?$fieldValue['value']:'';
                            
                            // save node field
                            $className = '\\BowtieFW\\Models\\'.$nodeDataField['storageTable'];
                            $objNodeData = new $className;
                            $objNodeData->save($saveData);
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        return $nodeId;
        
    }
    
}

?>