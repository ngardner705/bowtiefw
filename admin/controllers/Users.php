<?php
namespace BowtieFW\Admin;

class Users extends \BowtieFW\Controller {
    
    public $messages;
    private $userModel;
    
    function __construct() {
        
        parent::__construct();
        $this->userModel = new \BowtieFW\Models\User;
        $this->messages = array();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $permissionModel = new \BowtieFW\Models\Permission;
        $userList = $this->userModel->getAll();
        
        if(!empty($userList)) {
            foreach($userList as &$user) {
                $user['groups'] = $permissionModel->groupUsers->getUserGroups($user['id']);
            }
        }
        
        $this->view->assign('userList',$userList);
        $this->view->assign('content',$this->view->fetch('users/users.tpl.php'));
        $this->finish();
        
    }
    
    function actionEdit($params='') {
        
        // save changes
        if(!empty($params['saveuser']) && !empty($params['user_id'])) {
            
            // handle image upload
            //if(!empty($params['uploads']['avatar']['size'])) {
            //    $objFile = new \BowtieFW\File;
            //    $avatarInfo = $objFile->upload($params['uploads']['avatar'],'avatars');
            //} else {
            //    $avatarInfo = array('file'=>'common/img/default-user.jpg');
            //}
            
            // save them
            $saveData = array();
            $saveData['id'] = $params['user_id'];
            $saveData['email'] = !empty($params['email'])?$params['email']:null;
            //$saveData['firstname'] = !empty($params['firstname'])?$params['firstname']:null;
            //$saveData['lastname'] = !empty($params['lastname'])?$params['lastname']:null;
            //$saveData['company'] = !empty($params['company'])?$params['company']:null;
            //$saveData['title'] = !empty($params['title'])?$params['title']:null;
            //if(!empty($avatarInfo)) { $saveData['avatar'] = $avatarInfo['file']; }
            $saved = $this->userModel->save($saveData);
            
            if($saved) {
                
                $this->messages[] = array("type"=>"success","message"=>"User saved");
                
            } else {
                
                $this->messages[] = array("type"=>"error","message"=>"Unable to save user");
                
            }
            
        }
        
        if(!empty($params['user_id'])) {
            
            $usersDetails = $this->userModel->get($params['user_id']);
            
        } else {
            
            $usersDetails = array();
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('userDetails',$usersDetails);
        $this->view->assign('content',$this->view->fetch('users/edit.tpl.php'));
        $this->finish();
        
    }
    
    function actionDelete($params='') {
        
        if(!empty($params['confirmdelete']) && !empty($params['user_id'])) {
            
            $deleted = $this->userModel->deleteUser($params['user_id']);
            
            if($deleted) {
                $this->redirect('admin/users');
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Unable to delete user");
            }
            
        }
        
        if(!empty($params['user_id'])) {
            $usersDetails = $this->userModel->get($params['user_id']);
            $this->view->assign('messages',$this->messages);
            $this->view->assign('userDetails',$usersDetails);
            $this->view->assign('content',$this->view->fetch('users/delete.tpl.php'));
            $this->finish();
        } else {
            $this->messages[] = array("type"=>"error","message"=>"Unknown user");
            $this->view->assign('messages',$this->messages);
            $this->finish();
        }
        
    }
    
}

?>