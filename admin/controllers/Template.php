<?php
namespace BowtieFW\Admin;

class Template extends \BowtieFW\Controller {
    
    public $messages;
    private $templateModel;
    
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        $this->templateModel = new \BowtieFW\Models\Template;
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $templates = $this->templateModel->getAll();
        
        // put in groups
        $templateGroups = array();
        if(!empty($templates)) {
            foreach($templates as $aTemplate) {
                $templateGroups[$aTemplate['group']][] = $aTemplate;
            }
        }
        
        $this->view->assign('templateGroups',$templateGroups);
        $this->view->assign('content',$this->view->fetch('template/templates.tpl.php'));
        $this->finish();
        
    }
    
    function actionEdit($params='') {
        
        $templateId = !empty($params['template_id'])?intval($params['template_id']):0;
        
        if(!empty($params['dosave'])) {
            
            // save changes
            if(!empty($params['template']) && is_array($params['template'])) {
                $saveData = $params['template'];
                $templateId = $this->templateModel->save($saveData);
            }
            
        }
        
        if(!empty($templateId)) {
            
            // load template
            $templateInfo = $this->templateModel->get($templateId);
            
        } else {
            
            $templateInfo = array(
                'id'=>null,
                'title'=>null,
                'description'=>null,
                'content'=>null,
                'keyName'=>null,
                'group'=>null,
                'sortOrder'=>null
            );
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('template',$templateInfo);
        $this->view->assign('content',$this->view->fetch('template/edit.tpl.php'));
        $this->finish();
        
    }
    
}

?>