<?php
namespace BowtieFW\Admin;

class Css extends \BowtieFW\Controller {
    
    public $messages;
    
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        $this->cssModel = new \BowtieFW\Models\Css;
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $this->view->assign('stylesheets',$this->cssModel->loadStylesheets());
        $this->view->assign('content',$this->view->fetch('styles/css.tpl.php'));
        $this->finish();
        
    }
    
    function actionEdit($params='') {
        
        if(isset($params['_extra'][0])){
            $fileContents = $this->cssModel->loadStylesheet($params['_extra'][0]);
            if($fileContents){
                $this->view->assign('fileContents',$fileContents);
                $this->view->assign('fileName', $params['_extra'][0]);
            }
        }

        if(isset($params['doedit'])){
            //edit current CSS file
            $resp = $this->cssModel->saveStylesheet($params['filename'],$params['styles']);
            if($resp){
                $fileContents = $this->cssModel->loadStylesheet($resp);
                $this->view->assign('fileContents',$fileContents);
                $this->view->assign('fileName', $resp);
                $this->messages[] = array("type"=>"success","message"=>"CSS file updated successfully.");
            }else{
                $this->view->assign('fileContents',$params['styles']);
                $this->view->assign('fileName', $params['filename']);
                $this->messages[] = array("type"=>"error","message"=>"There was a problem updating this CSS file.");
            }

        }

        if(isset($params['dosave'])){
            //save new CSS file
            $resp = $this->cssModel->saveStylesheet($params['filename'],$params['styles']);
            if($resp){
                $fileContents = $this->cssModel->loadStylesheet($resp);
                $this->view->assign('fileContents',$fileContents);
                $this->view->assign('fileName', $resp);
                $this->messages[] = array("type"=>"success","message"=>"CSS file created successfully.");
            }else{
                $this->view->assign('fileContents',$params['styles']);
                $this->view->assign('fileName', $params['filename']);
                $this->messages[] = array("type"=>"error","message"=>"There was a problem creating this CSS file.");
            }
        }

        $this->view->assign('messages',$this->messages);
        $this->view->assign('content',$this->view->fetch('styles/edit.tpl.php'));
        $this->finish();
        
    }
    
}

?>