<?php
// Site Pages controller
namespace BowtieFW\Admin;

class Pages extends \BowtieFW\Controller {
    
    function __construct() {
        
        parent::__construct();
        $this->auth = \BowtieFW\Authentication::getInstance();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $objPage = new \BowtieFW\Models\Page;
        $pageList = $objPage->getAll();
        $pageRelatedDataList = array();
        foreach($pageList as &$page) {
            $pageRelatedDataList[] = $objPage->getRelatedData($page['id']);
        }
        
        $this->view->assign('contentPages',$pageRelatedDataList);
        $this->view->assign('content',$this->view->fetch('pages/pages.tpl.php'));
        $this->finish();
        
    }
    
    function actionNew($params='') {
        
        $objLayouts = new \BowtieFW\Models\Layout;
        $objPage = new \BowtieFW\Models\Page;
        $objRoute = new \BowtieFW\Models\Route;
        
        $pageId = false;
        $routeId = false;
        
        // save it and goto edit
        if(!empty($params['savepage'])) {
            
            $pageId = $this->savePage($params);
            
            $this->actionEdit(array('id'=>$pageId));
            return;
            
        }
        
        // Pages
        $pageList = $objPage->getAll();
        
        // Layouts
        $layoutList = $objLayouts->getAll();
        
        $this->view->assign('pageList',$pageList);
        $this->view->assign('layoutList',$layoutList);
        $this->view->assign('content',$this->view->fetch('pages/newpage.tpl.php'));
        $this->finish();
        
    }
    
    function actionEdit($params='') {
        
        $objPage = new \BowtieFW\Models\Page;
        $objLayouts = new \BowtieFW\Models\Layout;
        $objNode = new \BowtieFW\Models\Node;
        $objNodeInstance = new \BowtieFW\Models\NodeInstance;
        
        if(!empty($params['savepage'])) {
            $params['id'] = $this->savePage($params);
        }
        
        // Page Info
        $pageId = !empty($params['id'])?intval($params['id']):false;
        if(!$pageId) {
            $this->actionNew();
            return;
        } else {
            $pageInfo = $objPage->getRelatedData($pageId);
        }
        
        
        // Page placeholders and node instances
        $pagePlaceholders = array();
        if(!empty($pageInfo['layout']['content'])) {
            $bowtieTags = $this->view->findBowtieTags($pageInfo['layout']['content']);
            if(!empty($bowtieTags)) {
                foreach($bowtieTags as $bowtieTag) {
                    if($bowtieTag['tag'] == 'placeholder') {
                        $pageContents = $objPage->pageContent->find(array('page_id'=>$pageInfo['id'],'placeholderKey'=>$bowtieTag['params']['key']));
                        $pagePlaceholders[$bowtieTag['params']['key']] = array('nodeInstances'=>array());
                        if(!empty($pageContents)) {
                            $pagePlaceholders[$bowtieTag['params']['key']] = array('nodeInstances'=>array());
                            foreach($pageContents as $pageContent) {
                                $nodeData = $objNodeInstance->getRelatedData($pageContent['nodeInstance_id']);
                                if(!empty($nodeData)) {
                                    $pagePlaceholders[$bowtieTag['params']['key']]['nodeInstances'][] = $nodeData;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Available nodes to use
        $allNodes = $objNode->find(array('status'=>'published'));
        foreach($allNodes as &$theNode) {
            $theNode = $objNode->getRelatedData($theNode['id']);
        }
        
        // Pages
        $pageList = $objPage->getAll();
        
        // Layouts
        $layoutList = $objLayouts->getAll();
        
        $this->view->assign('pageInfo',$pageInfo);
        $this->view->assign('layoutList',$layoutList);
        $this->view->assign('pageList',$pageList);
        $this->view->assign('pagePlaceholders',$pagePlaceholders);
        $this->view->assign('availableNodes',$allNodes);
        $this->view->assign('content',$this->view->fetch('pages/page.tpl.php'));
        $this->finish();
        
    }
    
    /*
     * Save the page and its url
     * does not handle pageContents
     */
    function savePage($saveData) {
        
        $objPage = new \BowtieFW\Models\Page;
        $objRoute = new \BowtieFW\Models\Route;
        
        $pageId = false;
        $routeId = false;
        
        $saveRecord = array(
            'title'=>!empty($saveData['page']['title'])?$saveData['page']['title']:'New Page',
            'description'=>!empty($saveData['page']['description'])?$saveData['page']['description']:'',
            'layout_id'=>!empty($saveData['page']['layout_id'])?$saveData['page']['layout_id']:1,
            'status'=>!empty($saveData['page']['status'])?$saveData['page']['status']:'draft',
            'parent_id'=>!empty($saveData['page']['parent_id'])?$saveData['page']['parent_id']:0,
            'sortOrder'=>!empty($saveData['page']['sortOrder'])?$saveData['page']['sortOrder']:0
        );
        
        // update existing
        if(!empty($saveData['id'])) { $saveRecord['id'] = $saveData['id']; }
        
        $pageId = $objPage->save($saveRecord);
        
        if($pageId) {
            
            // save route
            $saveRecord = array(
                'url'=>$saveData['page']['url'],
                'context'=>'frontend',
                'controller'=>'page',
                'action'=>'index',
                'params'=>serialize(array('page_id'=>$pageId))
            );
            
            // update existing route
            if(!empty($saveData['page']['route_id'])) { $saveRecord['id'] = $saveData['page']['route_id']; }
            
            $routeId = $objRoute->save($saveRecord);
            
        }
        
        // url to route_id
        if($routeId) {
            $saveRecord = array(
                'id'=>$pageId,
                'route_id'=>$routeId
            );
            $objPage->save($saveRecord);
        }
        
        if($pageId) {
            return $pageId;
        } else {
            return false;
        }
        
    }
    
}

?>