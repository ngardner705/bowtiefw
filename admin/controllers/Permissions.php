<?php
namespace BowtieFW\Admin;

class Permissions extends \BowtieFW\Controller {
    
    public $messages;
    private $permissionModel;
    
    function __construct() {
        
        parent::__construct();
        $this->permissionModel = new \BowtieFW\Models\Permission;
        $this->messages = array();
        
    }
    
    // default action
    function actionIndex($params='') {
        
        if(!empty($params['savepermissions']) && !empty($params['grantpermission'])) {
            
            $permissions = array();
            
            foreach($params['grantpermission'] as $permissionId=>$groups) {
                foreach($groups as $groupId=>$nothing) {
                    $permissions[] = array('permission_id'=>$permissionId,'group_id'=>$groupId);
                }
            }
            
            $permissionsSet = $this->permissionModel->groupPermissions->setAllPermissions($permissions);
            
            if($permissionsSet) {
                $this->messages[] = array("type"=>"success","message"=>"Permissions saved");
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Unable to save permissions");
            }
            
        }
        
        $allPermissions = $this->permissionModel->getAllPermissions();
        $permissionGroups = $this->permissionModel->groups->getAll();
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('allPermissions',$allPermissions);
        $this->view->assign('allGroups',$permissionGroups);
        $this->view->assign('content',$this->view->fetch('permissions/permissions.tpl.php'));
        $this->finish();
        
    }
    
    function actionGroups($params='') {
        
        $groupList = $this->permissionModel->groups->getAll();
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('groupList',$groupList);
        $this->view->assign('content',$this->view->fetch('permissions/groups.tpl.php'));
        $this->finish();
        
    }
    
    function actionGroup($params='') {
        
        if(!empty($params['savegroup'])) {
            
            if(!empty($params['title'])) {
                
                $saveData = array('title'=>$params['title'],'keyName'=>$params['keyName'],'description'=>$params['description']);
                if(!empty($params['group_id'])) { $saveData['id'] = $params['group_id']; }
                
                $saved = $this->permissionModel->groups->save($saveData);
                
                if($saved) {
                    $this->messages[] = array("type"=>"success","message"=>"Group saved");
                    return $this->actionGroups();
                } else {
                    $this->messages[] = array("type"=>"error","message"=>"Unable to save group");
                }
                
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Group name is required");
            }
            
        }
        
        if(!empty($params['group_id'])) {
            $groupInfo = $this->permissionModel->groups->get($params['group_id']);
        } else {
            $groupInfo['id'] = 0;
            $groupInfo['title'] = '';
            $groupInfo['keyName'] = '';
            $groupInfo['description'] = '';
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('groupInfo',$groupInfo);
        $this->view->assign('content',$this->view->fetch('permissions/group.tpl.php'));
        $this->finish();
        
    }
    
    function actionDeleteGroup($params='') {
        
        if(!empty($params['group_id']) && $params['group_id'] > 3) {
            $deleted = $this->permissionModel->groups->deleteGroup($params['group_id']);
            if($deleted) {
                $this->messages[] = array("type"=>"success","message"=>"Group deleted");
            } else {
                $this->messages[] = array("type"=>"error","message"=>"Unable to delete group");
            }
        } else {
            $this->messages[] = array("type"=>"warning","message"=>"Unknown group");
        }
        
        return $this->actionGroups();
        
    }
    
    function actionUser($params='') {
        
        $auth = \BowtieFW\Authentication::getInstance();
        
        if(empty($params['user_id'])) {
            $this->redirect('admin/page/error?error=Unknown%20User');
        } else if($params['user_id'] == $auth->user_id) {
            $this->redirect('admin/page/error?error=Cannot%20modify%20your%20own%20permissions');
        } else {
            
            if(!empty($params['savepermissions'])) {
                
                $params['grantgroup'] = !empty($params['grantgroup'])?$params['grantgroup']:array();
                $setPermissions = $this->permissionModel->groupUsers->setUserGroups($params['user_id'],$params['grantgroup']);
                if($setPermissions) {
                    $this->messages[] = array("type"=>"success","message"=>"User's groups set");
                } else {
                    $this->messages[] = array("type"=>"error","message"=>"Unable to set permissions");
                }
                
            }
            
            $userModel = new \BowtieFW\Models\User;
            $userDetails = $userModel->get($params['user_id']);
            $permissionGroups = $this->permissionModel->groups->getAll();
            $userGroups = $this->permissionModel->groupUsers->getUserGroups($params['user_id']);
            
            foreach($permissionGroups as &$permGroup) {
                $permGroup['isMember'] = 0;
                foreach($userGroups as $userGroup) {
                    if($userGroup['id'] == $permGroup['id']) {
                        $permGroup['isMember'] = 1;
                        break;
                    }
                }
            }
            
            $this->view->assign('messages',$this->messages);
            $this->view->assign('userDetails',$userDetails);
            $this->view->assign('permissionGroups',$permissionGroups);
            $this->view->assign('content',$this->view->fetch('permissions/user.tpl.php'));
            $this->finish();
            
        }
        
    }
    
    function actionGenerate($params='') {
        
        $this->permissionModel->generatePermissions('frontend');
        $this->permissionModel->generatePermissions('admin');
        
        // reload your permissions
        $permissions = new \BowtieFW\Permission;
        $permissions->setPermissions(true);
        $this->messages[] = array("type"=>"success","message"=>"Permissions for frontend and admin generated");
        return $this->actionIndex();
        
    }
    
}

?>