<?php
namespace BowtieFW\Admin;

class Login extends \BowtieFW\Controller {
    
    public $messages;
    
    function __construct() {
        
        parent::__construct();
        $this->auth = \BowtieFW\Authentication::getInstance();
        $this->messages = array();
        
    }
    
    // custom layout for admin login
    function setLayout($layout = 'login.tpl.php') {
        
        $this->layout = $layout;
        
    }
    
    // default action
    function actionIndex($params='') {
        
        if(!empty($params['loginattempt'])) {
            
            if(!empty($params['username']) && !empty($params['password'])) {
                
                $rememberMe = !empty($params['rememberme'])?true:false;
                
                $loginSuccess = $this->auth->login($params['username'],$params['password'],$rememberMe);
                
                if($loginSuccess) {
                    
                    $redirect = !empty($params['afterlogin'])?$params['afterlogin']:'admin';
                    $this->redirect($redirect);
                    
                } else {
                    
                    $this->messages[] = end($this->auth->messages); // get auth's last message
                    
                }
                
            } else {
                
                $this->messages[] = array("type"=>"warning","message"=>"Username and Password are required");
                
            }
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->finish();
        
    }
    
}

?>