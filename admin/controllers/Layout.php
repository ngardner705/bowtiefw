<?php
namespace BowtieFW\Admin;

class Layout extends \BowtieFW\Controller {
    
    public $messages;
    private $layoutModel;
    
    function __construct() {
        
        parent::__construct();
        $this->messages = array();
        $this->layoutModel = new \BowtieFW\Models\Layout;
        
    }
    
    // default action
    function actionIndex($params='') {
        
        $layouts = $this->layoutModel->getAll();
        
        $this->view->assign('layouts',$layouts);
        $this->view->assign('content',$this->view->fetch('layout/layouts.tpl.php'));
        $this->finish();
        
    }
    
    function actionNew($params='') {
        
        $this->actionEdit();
        
    }
    
    function actionEdit($params='') {
        
        $layoutId = !empty($params['layout_id'])?intval($params['layout_id']):0;
        
        if(!empty($params['dosave'])) {
            
            // save changes
            if(!empty($params['layout']) && is_array($params['layout'])) {
                $saveData = $params['layout'];
                $layoutId = $this->layoutModel->save($saveData);
            }
            
        }
        
        if(!empty($layoutId)) {
            
            // load layout
            $layoutInfo = $this->layoutModel->get($layoutId);
            $layoutInfo['layoutAssets'] = $this->layoutModel->layoutAssets->find(array("layout_id"=>$layoutInfo['id']));
            
        } else {
            
            $layoutInfo = array(
                'id'=>null,
                'title'=>null,
                'description'=>null,
                'content'=>null,
                'layoutAssets'=>array()
            );
            
        }
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('layout',$layoutInfo);
        $this->view->assign('content',$this->view->fetch('layout/edit.tpl.php'));
        $this->finish();
        
    }
    
}

?>