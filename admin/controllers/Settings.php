<?php
namespace BowtieFW\Admin;

class Settings extends \BowtieFW\Controller {
    
    public $messages;
    private $siteSettingModel;
    
    function __construct() {
        
        parent::__construct();
        $this->siteSettingModel = new \BowtieFW\Models\Sitesetting;
        $this->settings = \BowtieFW\Sitesetting::getInstance();
        $this->messages = array();
        
    }
    
    // default action
    function actionIndex($params=''){

        if(isset($params['dosave'])){
            $successCount = 0;
            $failCount = 0;
            $settings = $params['setting'];
            $compareSettings = array();
            foreach($this->settings->getEntries() as $entry){
                foreach ($entry as $key => $value){
                    $compareSettings[$key] = $value;
                }
            }
            foreach($settings as $key => $value){
                if($compareSettings[$key]['value'] != $value){
                    if($this->siteSettingModel->updateValue($key, $value)){
                        $successCount++;
                    }else{
                        $failCount++;
                    }
                }
            }
            if($failCount == 0){
                $this->messages[] = array("type"=>"success","message"=>"All Settings Saved Successfully");
            }else{
                $this->messages[] = array("type"=>"error","message"=>"$failCount Errors, $successCount Settings Saved Successfully");
            }
        }

        $this->settings->loadSettings();
        
        $this->view->assign('messages',$this->messages);
        $this->view->assign('siteSettings',$this->settings->getEntries());
        $this->view->assign('content',$this->view->fetch('settings/settings.tpl.php'));
        $this->finish();
        
    }

    // add new setting
    function actionEdit($params=''){

        if(isset($params['delete'])){
            if($this->siteSettingModel->delete($params['optid'])){
                $this->messages[] = array("type"=>"success","message"=>"Setting deleted successfully");
            }else{
                $this->messages[] = array("type"=>"error","message"=>"There was a problem deleting this setting");
                $this->view->assign('setting', $this->settings->getEntry($params['group'],$params['key']));
            }
        }

        if(isset($params['dosave'])){

            $saveData['title'] = !empty($params['title']) ? $params['title'] : 'Untitled Setting';
            $saveData['description'] = $params['description'];
            $saveData['key'] = $this->isUnique($params['key']) ? $params['key'] : $this->makeUnique($params['key']);
            $saveData['value'] = $params['value'];
            $saveData['options'] = $params['options'];
            $saveData['group'] = !empty($params['group']) ? $params['group'] : "Ungrouped";
            if($this->siteSettingModel->save($saveData)){
                $this->settings->loadSettings();
                $this->view->assign('setting', $this->settings->getEntry($saveData['group'],$saveData['key']));
                $this->messages[] = array("type"=>"success","message"=>"Setting added successfully");
            }else{
                $this->view->assign('setting', $this->settings->getEntry($params['group'],$params['key']));
                $this->messages[] = array("type"=>"error","message"=>"There was a problem adding your new setting");
            }

        }

        if(isset($params['doedit'])){

            $saveData['title'] = !empty($params['title']) ? $params['title'] : 'Untitled Setting';
            $saveData['description'] = $params['description'];
            $saveData['key'] = $params['key'];
            $saveData['value'] = $params['value'];
            $saveData['options'] = $params['options'];
            $saveData['group'] = !empty($params['group']) ? $params['group'] : "Ungrouped";
            if($this->siteSettingModel->updateSetting($saveData)){
                $this->settings->loadSettings();
                $this->view->assign('setting', $this->settings->getEntry($saveData['group'],$saveData['key']));
                $this->messages[] = array("type"=>"success","message"=>"Setting updated successfully");
            }else{
                $this->view->assign('setting', $this->settings->getEntry($params['group'],$params['key']));
                $this->messages[] = array("type"=>"error","message"=>"There was a problem editing this setting");
            }

        }

        if(isset($params['_extra'][0]) && isset($params['_extra'][1])){
            $this->view->assign('setting', $this->settings->getEntry($params['_extra'][0],$params['_extra'][1]));
        }
        
        $this->view->assign('messages',$this->messages);
        $this->settings->loadSettings();
        $this->view->assign('siteSettings',$this->settings->getEntries());
        $this->view->assign('content',$this->view->fetch('settings/edit.tpl.php'));
        $this->finish();

    }

    function actionAjaxIsUnique($params=''){
        if(isset($params['key'])){
            $this->JSONoutput($this->siteSettingModel->keyUnique($params['key']));
        }
    }

    function isUnique($key){
        return $this->siteSettingModel->keyUnique($key);
    }
    
    function makeUnique($key){
        $i = 0;
        if($this->isUnique($key)){
            return $key;
        }else{
            $f = true;
            while($f){
                $i ++;
                if($this->isUnique($key.'-'.$i)){
                    $uKey = $key.'-'.$i;
                    $f = false;
                    return $uKey;
                }
            }
        }
    }

}

?>