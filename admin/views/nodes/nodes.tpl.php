<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/node/edit?nodeType=<?=$nodeType['keyName'];?>">Create <?=$nodeType['title'];?> Node</a></li>
    </ul>
</div>

<h1>Manage <?=$nodeType['title'];?> Nodes</h1>

<div class="row">
    <div class="col3"><div class="pad10"><strong>Title</strong></div></div>
    <div class="col5"><div class="pad10"><strong>Description</strong></div></div>
    <div class="col2"><div class="pad10"><strong>Type</strong></div></div>
    <div class="col2"><div class="pad10"><strong>Status</strong></div></div>
</div>
<?php foreach($nodeList as $aNode) { ?>
<div class="row hover-highlight">
    <div class="col3"><div class="pad10"><a href="<?=$DIR_INSTALL;?>admin/node/edit?id=<?=$aNode['id'];?>"><?=$aNode['title'];?></a></div></div>
    <div class="col5"><div class="pad10"><?=$aNode['description'];?></div></div>
    <div class="col2"><div class="pad10"><?=$aNode['nodeType']['title'];?></div></div>
    <div class="col2"><div class="pad10"><?=$aNode['status'];?></div></div>
</div>
<?php } ?>