<h1>Editing Node</h1>

<form method="post" action="<?=$DIR_INSTALL;?>admin/node/edit">
<input type="hidden" name="dosave" value="1"/>
    <fieldset>
        <legend>Basic Info</legend>
        <input type="hidden" name="node[id]" value="<?=$nodeInfo['id'];?>"/>
        <div class="field">
            <label for="node_title">Title</label>
            <input type="text" name="node[title]" id="node_title" placeholder="Title" value="<?=$nodeInfo['title'];?>"/>
        </div>
        <div class="field">
            <label for="node_description">Description</label>
            <input type="text" name="node[description]" id="node_description" placeholder="Description" value="<?=$nodeInfo['description'];?>"/>
        </div>
        <div class="field">
            <label for="node_status">Status</label>
            <select name="node[status]" id="node_status">
                <option value="draft" <?=($nodeInfo['status']=='draft')?'selected="selected"':'';?>>Draft</option>
                <option value="published" <?=($nodeInfo['status']=='published')?'selected="selected"':'';?>>Published</option>
            </select>
        </div>
    </fieldset>
    
    <fieldset>
        <legend><?=$nodeInfo['nodeType']['title'];?> Fields</legend>
        <input type="hidden" name="node[nodeType_id]" value="<?=$nodeInfo['nodeType']['id'];?>"/>
        <?php foreach($nodeInfo['nodeFields'] as $nodeField) { ?>
        <div class="field <?=$nodeField['fieldType'];?>">
            <label for="nodedata_<?=$nodeField['keyName'];?>"><?=$nodeField['title'];?></label>
            <?php include 'fieldtypes/'.strtolower($nodeField['fieldType']).'.tpl.php'; ?>
        </div>
        <?php } ?>
    </fieldset>
    
    <input type="submit" value="Save"/>
    
</form>