<h1>Site Settings</h1><a class="add-new" href="<?=$DIR_INSTALL;?>admin/settings/edit">Add</a>
<form method="post" action="<?=$DIR_INSTALL;?>admin/settings">
    <input type="hidden" name="dosave" value="1"/>
    <?php $group = ""; foreach($siteSettings as $key => $value):
        if($key != $group):
            $group = $key;?>
            <h4 class="settings-group"><?=$group?></h4>
        <?php endif; 
        if(is_array($value)):
            foreach($value as $v): ?>
                <div class="row hover-highlight">
                    
                    <div class="col2">
                        <label for="<?=$v['key']?>"><?=$v['title']?></label>
                    </div>
                    <div class="col9">
                        <?php if(isset($v['options'])):?>
                            <select name="setting[<?=$v['key']?>]" class="setting-options" id="<?=$v['key']?>">
                                <?php if($v['value'] == ""): ?>
                                    <option value="" disabled selected>Make a selection</option>
                                <?php endif;
                                $options = explode('|', $v['options']);
                                foreach($options as $option):
                                    if($option == $v['value']): ?>
                                        <option value="<?=$option?>" selected><?=$option?></option>
                                    <?php else: ?>
                                        <option value="<?=$option?>"><?=$option?></option>
                                    <?php endif;
                                endforeach; ?>
                            </select>
                        <?php else: ?>
                            <input class="settings-value" type="text" name="setting[<?=$v['key']?>]" value="<?=$v['value']?>" id="<?=$v['key']?>" />
                        <?php endif; ?>
                        <small class="settings-description"><?=$v['description']?></small>
                    </div>
                    <div class="col1">
                        <a class="edit" href="<?=$DIR_INSTALL;?>admin/settings/edit/<?=$group?>/<?=$v['key']?>">Edit</a>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif ?>
    <?php endforeach; ?>
    <input type="submit" value="Update"/>
</form>