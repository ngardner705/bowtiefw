<?php
$e = false;
if(isset($setting)){
    $e = true;
}
?>
<h1><?php echo $e ? 'Edit' : 'New' ?> Site Setting</h1>
<?php if($e): ?>
    <form id="delete-setting" method="POST" action="<?=$DIR_INSTALL;?>admin/settings/edit" enctype="multipart/form-data">
        <input type="hidden" name="delete" value="1" />
        <input type="hidden" name="optid" value="<?=$setting['id']?>" />
        <input type="submit" value="Delete Setting" />
    </form>
<?php endif; ?>
<form id="edit-setting" method="POST" action="<?=$DIR_INSTALL;?>admin/settings/edit" enctype="multipart/form-data">
    <input type="hidden" name="<?php echo $e ? 'doedit' : 'dosave' ?>" value="1"/>
    <fieldset>
        <legend>Setting Info</legend>
        <div class="row">
            <div class="col6">
                <div class="row">
                    <div class="col6">
                        <label for="title">Title</label>
                    </div>
                    <div class="col6">
                        <input type="text" name="title" id="title" value="<?php echo $e ? $setting['title'] : '' ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="col6">
                        <label for="value">
                            Value
                            <small>Setting value, can be changed later [optional]</small>
                        </label>
                    </div>
                    <div class="col6">
                        <input type="text" name="value" id="value" value="<?php echo $e ? $setting['value'] : '' ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="col6">
                        <label for="key">
                            Name
                            <small>For developers. No caps, spaces, or special characters. Must be unique.</small>
                        </label>
                    </div>
                    <div class="col6">
                        <input type="text" name="key" id="key" readonly value="<?php echo $e ? $setting['key'] : '' ?>" /><a href="javascript:void(0)" class="edit edit-readonly" data-id="key">Edit</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col6">
                        <label for="group">
                            Setting Group
                            <small>Choose from existing groups or create one</small>
                        </label>
                    </div>
                    <div class="col6">
                        <select name="group" id="group">
                            <option disabled selected>-- Select One --</option>
                            <?php $group = ""; foreach($siteSettings as $key => $value):
                                if($key != $group):
                                    $group = $key;?>
                                        <?php if($e && $group == $setting['group']):?>
                                            <option value="<?=$group?>" selected><?=$group?></h4>
                                        <?php else: ?>
                                            <option value="<?=$group?>"><?=$group?></h4>
                                        <?php endif; ?>
                                <?php endif;
                            endforeach; ?>
                        </select>
                        <input type="text" id="add-group" name="add-group" /><a id="add-group-button" class="add-button" href="javascript:void(0)">Add</a>
                    </div>
                </div>
            </div>
            <div class="col6">
                <div class="row">
                    <div class="col6">
                        <label for="description">Description</label>
                    </div>
                    <div class="col6">
                        <textarea name="description" id="description"><?php echo $e ? $setting['description'] : '' ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col6">
                        <label for="options">
                            Options
                            <small>Add possible setting options [optional]</small>
                        </label>
                    </div>
                    <div class="col6">
                        <div id="setting-options">
                            <input type="hidden" name="options" id="options" value="<?php echo ($e && isset($setting['options'])) ? $setting['options'] : '' ?>" />
                                <?php if($e && isset($setting['options'])):
                                    $options = explode('|', $setting['options']);
                                    foreach ($options as $option): ?>
                                        <input type="text" class="setting-option" value="<?=$option?>" />
                                    <?php endforeach; 
                                else: ?>
                                    <input type="text" class="setting-option" />
                                <?php endif; ?>
                            <a href="javascript:void(0)" id="add-option" class="add-button">Add</a>
                        </div>
                    </div>
            </div>
            <input type="submit" value="Save "
        </div>
    </fieldset>
</form>