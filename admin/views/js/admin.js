//object of string operations used by Sitesettings
$.stringOps = {
    //properties
    i: 0,
    //methods
    makeURLSafe: function(str){
        //lowercase, spaces to dashes, no special characters
        return str.toLowerCase().replace(/ /g,'-').replace(/[^a-z0-9-]/gi,'').replace(/-{2,}/g,'-');
    },
    isUnique: function(str){
        //AJAX check if key value is unique return true/false
        var resp;
        $.ajax({
            url: '/bowtiefw/bowtiefw/admin/settings/ajaxisunique/?key='+str,
            success: function(d){
                if(d){
                    resp = true;
                }else{
                    resp = false;
                }
            },
            async: false
        });
        return resp;
    },
    makeUnique: function(str){
        //AJAX check if str already exists as a key in the settings table, if so change it to be unique
        if(this.isUnique(str)){
            return str;
        }else{
            var f = true;
            while(f){
                this.i ++;
                if(this.isUnique(str+'-'+this.i)){
                    var key = str+'-'+this.i
                    f = false;
                    this.i = 0;
                    return key;
                }
            }
        }
    },
    makeKeyName: function(str){
        //runs makeSafe and makeUnique
        str = this.makeURLSafe(str);
        str = this.makeUnique(str);
        return str;
    },
    combineOptions: function(){
        //combine options inputs into a single '|' delimited string
        var options;
        $('#setting-options input.setting-option').each(function(){
            if($(this).val() != ""){
                options += $(this).val()+'|';
            }
        });
        $('#setting-options #options').val(options.replace(/\|$/, ""));
    }
}

$(document).ready(function() {
    
    // Sitesettings
    (function($) {
        if($('#add-setting').length){
            $('#add-setting #title').on('change', function(){
                $('#add-setting #key').val($.stringOps.makeKeyName($(this).val()));
            });
            $('#add-group-button').on('click', function(){
                var g = $('#add-group').val();
                $('#group').append('<option value="'+g+'" selected>'+g+'</option>');
            });
            $('#add-option').on('click', function(){
                $(this).parent('div#setting-options').append('<input type="text" class="setting-option">');
            });
            $('#setting-options').on('change', $.stringOps.combineOptions);
        }
        $('a.edit-readonly').on('click', function(){
            $('#'+$(this).data('id')).removeAttr('readonly');
        });
    })(jQuery);

    (function($) {
        if($('#css-editor').length){
            var cm = CodeMirror.fromTextArea(document.getElementById('css-editor'),{
                lineNumbers: true,
                mode: "css",
                matchBrackets: true,
                styleActiveLine: true,
                highlightSelectionMatches: true
            });
        }
    })(jQuery);
    
    // main navigation
    (function($) {
        
        var timeout	= 500;
        var closeTimer = 0;
        var currentMenuItem = 0;
        
        // open hidden layer
        function openMenu(menuItem) {
            
            //console.log('opening menu');
            //console.log(menuItem);
            
            // cancel close timer
            cancelCloseTimer();
            
            // close old menu item
            if(currentMenuItem) {
                currentMenuItem.children('ul').hide();
            }
            
            // get new layer and show it
            currentMenuItem = $(menuItem);
            currentMenuItem.children('ul').show();
            
        }
        
        // close showed layer
        function closeMenu() {
            if(currentMenuItem) currentMenuItem.children('ul').hide();
        }
        
        // go close timer
        function startCloseTimer() {
            closeTimer = window.setTimeout(closeMenu, timeout);
        }
        
        // cancel close timer
        function cancelCloseTimer() {
            if(closeTimer) {
                window.clearTimeout(closeTimer);
                closeTimer = null;
            }
        }
        
        // attach events
        document.onclick = closeMenu;
        $('#header ul.mainnav li').on('mouseover',function() {
            openMenu(this);
        });
        $('#header ul.mainnav li ul').on('mouseout',function() {
            startCloseTimer()
        });
        
    })(jQuery);
    
    // jQuery UI
    (function($) {
        $('.jqueryui-tabs').tabs();
    })(jQuery);
    
    // Page editor
    (function($) {
        
    })(jQuery);
    
    // Layout editor
    (function($) {
        
        var addJsAssetDialog = $('#addJsAssetPopup').dialog({
            autoOpen: false,
            width: 500
        });
        $('.miniButton.addJsAsset').on('click',function() {
            addJsAssetDialog.dialog("open");
        });
        
        // add js asset form gets submitted via ajax
        $('#addJsAssetForm').submit(function(e) {
            
            var url = $(this).attr("action");
            var postData = $(this).serialize();
            
            $.ajax({
                type: "POST",
                url: url,
                data: postData,
                success: function(response) {
                    if (response.result == 'success') {
                        addJsAssetDialog.dialog("close"); // close form
                        $('#addJsAssetForm').trigger('reset'); // clear form
                        addJsAsset(response.response); // add to dom
                    } else {
                        alert("Error : " + response.response);
                    }
                },
                error: function() {
                    alert('Error saving JavaScript asset');
                },
                complete: function() {
                    
                }
            });
            
            e.preventDefault(); // dont let browser submit the form
            
        });
        
        function addJsAsset(newField) {
            
            var container = $('#dataFieldsContainer');
            
            var newHTML = '<div class="row hover-highlight">\
                <div class="col3"><div class="pad10">'+newField.title+'</div></div>\
                <div class="col6"><div class="pad10">'+newField.description+'</div></div>\
                <div class="col2"><div class="pad10">'+newField.fieldType+'</div></div>\
                <div class="col1"><div class="pad10">'+newField.required+'</div></div>\
            </div>';
            
            container.append(newHTML);
            
        }
        
    })(jQuery);
    
    // Content builder
    (function($) {
        var addFieldDialog = $('#popupContainer').dialog({
            autoOpen: false,
            width: 500
        });
        $('.miniButton.addField').on('click',function() {
            addFieldDialog.dialog("open");
        });
        
        // add field form gets submitted via ajax
        $('#addFieldForm').submit(function(e) {
            
            var url = $(this).attr("action");
            var postData = $(this).serialize();
            
            $.ajax({
                type: "POST",
                url: url,
                data: postData,
                success: function(response) {
                    if (response.result == 'success') {
                        addFieldDialog.dialog("close"); // close form
                        $('#addFieldForm').trigger('reset'); // clear form
                        addDataField(response.response); // add to dom
                    } else {
                        alert("Error : " + response.response);
                    }
                },
                error: function() {
                    alert('Error saving form field');
                },
                complete: function() {
                    
                }
            });
            
            e.preventDefault(); // dont let browser submit the form
            
        });
        
        function addDataField(newField) {
            
            var container = $('#dataFieldsContainer');
            
            var newHTML = '<div class="row hover-highlight">\
                <div class="col3"><div class="pad10">'+newField.title+'</div></div>\
                <div class="col6"><div class="pad10">'+newField.description+'</div></div>\
                <div class="col2"><div class="pad10">'+newField.fieldType+'</div></div>\
                <div class="col1"><div class="pad10">'+newField.required+'</div></div>\
            </div>';
            
            container.append(newHTML);
            
        }
        
    })(jQuery);
    
});