<h1>Template</h1>

<form method="post" action="<?=$DIR_INSTALL;?>admin/template/edit">
<input type="hidden" name="dosave" value="1"/>
<input type="hidden" name="template[id]" value="<?=$template['id'];?>"/>
<fieldset>
    <legend>Template Info</legend>
    <div class="field">
        <label for="template_title">Title</label>
        <input type="text" name="template[title]" id="template_title" placeholder="New Template" value="<?=$template['title'];?>"/>
    </div>
    <div class="field">
        <label for="template_description">Description</label>
        <input type="text" name="template[description]" id="template_description" placeholder="Description for this template" value="<?=$template['description'];?>"/>
    </div>
    <div class="field">
        <label for="template_keyname">Keyname</label>
        <input type="text" name="template[keyName]" id="template_keyname" placeholder="Keyname for this template" value="<?=$template['keyName'];?>"/>
    </div>
    <div class="field">
        <label for="template_group">Group</label>
        <input type="text" name="template[group]" id="template_group" placeholder="Group for this template" value="<?=$template['group'];?>"/>
    </div>
</fieldset>
<hr/>
<fieldset>
    <legend>Template Code</legend>
    <div class="field">
        <label for="template_content">Body</label>
        <textarea name="template[content]" id="template_content"><?=htmlentities($template['content']);?></textarea>
    </div>
</fieldset>
<input type="submit" value="Save"/>
</form>