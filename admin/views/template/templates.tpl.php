<h1>Templates</h1>
<?php foreach($templateGroups as $templateGroupName=>$templates) { ?>
    <div class="row"><div class="col12"><h3><?=$templateGroupName;?></h3></div></div>
    <?php foreach($templates as $aTemplate) { ?>
        <div class="row hover-highlight">
            
            <div class="col2"><div class="pad10">
                <h5><?=$aTemplate['title'];?></h5>
            </div></div>
            
            <div class="col6"><div class="pad10">
                <p><?=$aTemplate['description'];?></p>
            </div></div>
            
            <div class="col1"><div class="pad10">
                <a href="<?=$DIR_INSTALL;?>admin/template/edit?template_id=<?=$aTemplate['id'];?>" class="miniButton">Edit</a><br/>
                <a href="<?=$DIR_INSTALL;?>admin/template/delete?template_id=<?=$aTemplate['id'];?>" class="miniButton">Delete</a><br/>
            </div></div>
            
        </div>
    <?php } ?>
    <hr/>
<?php } ?>