<!DOCTYPE html>

<html>
<head>
    <?php include 'head.tpl.php'; ?>
    <title><?=$PRODUCT_NAME;?></title>
</head>

<body>
    <?php include 'header.tpl.php'; ?>
    <div class="container">
        <?php include 'messages.tpl.php'; ?>
        <?php if(isset($content)) { echo $content; } else { echo '<p>No content defined</p>'; } ?>
    </div>
    
    <?php include 'footer.tpl.php'; ?>
    <?php include 'foot.tpl.php'; ?>
</body>
</html>