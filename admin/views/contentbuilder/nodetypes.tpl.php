<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/contentbuilder/new">Create new Node Type</a></li>
    </ul>
</div>

<h1>Manage Node Types</h1>

<div class="row">
    <div class="col3"><div class="pad10"><strong>Title</strong></div></div>
    <div class="col7"><div class="pad10"><strong>Description</strong></div></div>
    <div class="col2"><div class="pad10"><strong>Nodes</strong></div></div>
</div>
<?php foreach($nodeTypes as $nodeType) { ?>
<div class="row hover-highlight">
    <div class="col3"><div class="pad10"><a href="<?=$DIR_INSTALL;?>admin/contentbuilder/edit?id=<?=$nodeType['id'];?>"><?=$nodeType['title'];?></a></div></div>
    <div class="col7"><div class="pad10"><?=$nodeType['description'];?></div></div>
    <div class="col2"><div class="pad10"><?=$nodeType['numbNodes'];?></div></div>
</div>
<?php } ?>