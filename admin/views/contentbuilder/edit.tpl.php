<h1>Editing Node Type</h1>

<form method="post" action="<?=$DIR_INSTALL;?>admin/contentbuilder/edit">
<input type="hidden" name="dosave" value="1"/>
    <fieldset>
        <legend>Basic Info</legend>
        <input type="hidden" name="nodetype[id]" value="<?=$nodeTypeInfo['id'];?>"/>
        <div class="field">
            <label for="nodetype_title">Title</label>
            <input type="text" name="nodetype[title]" id="nodetype_title" placeholder="Title" value="<?=$nodeTypeInfo['title'];?>"/>
        </div>
        <div class="field">
            <label for="nodetype_description">Description</label>
            <input type="text" name="nodetype[description]" id="nodetype_description" placeholder="Description" value="<?=$nodeTypeInfo['description'];?>"/>
        </div>
        <div class="field">
            <label for="nodetype_keyname">Key Name</label>
            <input type="text" name="nodetype[keyName]" id="nodetype_keyname" placeholder="Internal key name" value="<?=$nodeTypeInfo['keyName'];?>" <?=($nodeTypeInfo['id'])?'readonly="readonly"':'';?>/>
        </div>
    </fieldset>
    
    <fieldset>
        <legend>Data Fields</legend>
        <?php if(!empty($nodeTypeInfo['id'])) { ?>
        
        <div class="row">
            <div class="col3">Field name</div>
            <div class="col6">Description</div>
            <div class="col2">Type</div>
            <div class="col1">Required</div>
        </div>
        
        <div id="dataFieldsContainer">
            <div class="row hover-highlight">
                <div class="col3"><div class="pad10">Title</div></div>
                <div class="col6"><div class="pad10">Title for this node</div></div>
                <div class="col2"><div class="pad10">text</div></div>
                <div class="col1"><div class="pad10">1</div></div>
            </div>
            <div class="row hover-highlight">
                <div class="col3"><div class="pad10">Description</div></div>
                <div class="col6"><div class="pad10">Short description of node</div></div>
                <div class="col2"><div class="pad10">text</div></div>
                <div class="col1"><div class="pad10">0</div></div>
            </div>
            <?php foreach($nodeFields as $nodeField) { ?>
            <div class="row hover-highlight">
                <div class="col3"><div class="pad10"><?=$nodeField['title'];?></div></div>
                <div class="col6"><div class="pad10"><?=$nodeField['description'];?></div></div>
                <div class="col2"><div class="pad10"><?=$nodeField['fieldType'];?></div></div>
                <div class="col1"><div class="pad10"><?=$nodeField['required'];?></div></div>
            </div>
            <?php } ?>
        </div>
        
        <div class="miniButton addField">+Add Field</div>
        
        <?php } else { ?>
        <p>Save to start adding fields.</p>
        <?php } ?>
        
    </fieldset>
    
    <input type="submit" value="Save"/>
    
</form>

<div id="popupContainer" title="Add Field" style="display: none;">
    <form method="post" action="<?=$DIR_INSTALL;?>admin/contentbuilder/ajaxAddField" id="addFieldForm">
        <input type="hidden" name="newfield[nodeType_id]" value="<?=$nodeTypeInfo['id'];?>"/>
        <div class="field">
            <label for="newfield_type">Field Type</label>
            <select name="newfield[fieldType]">
                <?php foreach($fieldOptions as $fieldOption) { ?>
                <option value="<?=$fieldOption['type'];?>"><?=$fieldOption['title'];?> (<?=$fieldOption['description'];?>)</option>
                <?php } ?>
            </select>
        </div>
        <div class="field">
            <label for="newfield_title">Title</label>
            <input type="text" name="newfield[title]" id="newfield_title" placeholder="Title for this field"/>
        </div>
        <div class="field">
            <label for="newfield_description">Description</label>
            <input type="text" name="newfield[description]" id="newfield_description" placeholder="Describe what this field will contain"/>
        </div>
        <div class="field">
            <label for="newfield_keyname">Key Name</label>
            <input type="text" name="newfield[keyName]" id="newfield_keyname" placeholder="Internal key name used to identify this field"/>
        </div>
        <div class="field">
            <label for="newfield_required">Required</label>
            <input type="checkbox" name="newfield[required]" id="newfield_required" value="1"/>
        </div>
        <div class="field">
            <label for="newfield_default">Default</label>
            <input type="text" name="newfield[default]" id="newfield_default" placeholder="Default value for this field"/>
        </div>
        <div class="field">
            <label for="newfield_options">Possible Options</label>
            <input type="text" name="newfield[options]" id="newfield_options" placeholder="Possible options to select from"/>
        </div>
        <div class="field">
            <label for="newfield_numbvalues">How many values</label>
            <input type="text" name="newfield[numbAllowedValues]" id="newfield_numbvalues" placeholder="0 = unlimited" value="1"/>
        </div>
        <input type="submit" value="Add Field"/>
    </form>
</div>