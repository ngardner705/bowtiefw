<!DOCTYPE html>

<html>
<head>
    <?php include 'head.tpl.php'; ?>
    <title><?=$PRODUCT_NAME;?> | Admin Login</title>
</head>

<body>
    
    <div class="container">
        <?php include 'messages.tpl.php'; ?>
        
        <div id="adminLogin">
            <form method="POST" action="<?=$DIR_INSTALL;?>admin/login">
                <input type="hidden" name="loginattempt" value="1"/>
                <div class="row">
                    <div class="col12"><div class="logo"><img src="<?=$DIR_INSTALL;?>common/img/logo.png" alt="<?=$PRODUCT_NAME;?>" title="<?=$PRODUCT_NAME;?>"/></div></div>
                </div>
                <div class="row">
                    <div class="col12"><input type="text" placeholder="Username" name="username"/></div>
                </div>
                <div class="row">
                    <div class="col12"><input type="password" placeholder="Password" name="password"/></div>
                </div>
                <div class="row">
                    <div class="col12"><input type="submit" class="button" value="Login"/></div>
                </div>
            </form>
        </div>
        
    </div>
    
    <?php include 'footer.tpl.php'; ?>
    <?php include 'foot.tpl.php'; ?>
</body>
</html>