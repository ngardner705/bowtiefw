<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/permissions/groups">Groups</a></li>
        <li><a href="<?=$DIR_INSTALL;?>admin/permissions/generate">Generate new permissions</a></li>
    </ul>
</div>

<h1>Manage Permissions</h1>

<form method="POST" action="<?=$DIR_INSTALL;?>admin/permissions" id="permissionsAdminForm">
    <input type="hidden" name="savepermissions" value="1"/>
        <div class="row">
            <div class="col1"><div class="pad10"><strong>Interface</strong></div></div>
            <div class="col1"><div class="pad10"><strong>Controller</strong></div></div>
            <div class="col1"><div class="pad10"><strong>Action</strong></div></div>
            <div class="col9"><div class="pad10"><strong>Groups</strong></div></div>
        </div>
        <?php foreach($allPermissions as $aPermission) { ?>
        <div class="row hover-highlight">
            <div class="col1"><div class="pad10"><?=$aPermission['context'];?></div></div>
            <div class="col1"><div class="pad10"><?=$aPermission['controller'];?></div></div>
            <div class="col1"><div class="pad10"><?=$aPermission['action'];?></div></div>
            <div class="col9">
                <div class="row">
                
                <?php
                $columns = floor(12/count($allGroups));
                $columns = 2;
                foreach($allGroups as $aGroup) {
                    if($aGroup['id'] == 3) {
                        $checked = 'checked="checked" disabled="disabled"';
                    } else {
                        $checked = '';
                        foreach($aPermission['groups'] as $permissionGroup) {
                            if($permissionGroup['id'] == $aGroup['id']) {
                                $checked = 'checked="checked"';
                                break;
                            }
                        }
                    }
                    ?>
                    <div class="col<?=$columns;?>"><div class="pad10">
                        <input type="checkbox" name="grantpermission[<?=$aPermission['id'];?>][<?=$aGroup['id'];?>]" <?=$checked;?>/> <?=$aGroup['title'];?>
                    </div></div>
                <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col12"><input type="submit" value="Save Permissions"/></div>
        </div>
</form>