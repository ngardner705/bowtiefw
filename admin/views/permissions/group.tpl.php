<h1>Permission Group</h1>
<form method="POST" action="<?=$DIR_INSTALL;?>admin/permissions/group">
    <input type="hidden" name="savegroup" value="1"/>
    <input type="hidden" name="group_id" value="<?=$groupInfo['id'];?>"/>
    <fieldset><legend>Information</legend>
        <div class="row">
            <div class="col4"><label for="group_title">Title</label></div>
            <div class="col8"><input type="text" placeholder="Title" name="title" id="group_title" value="<?=$groupInfo['title'];?>"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="group_keyname">KeyName</label></div>
            <div class="col8"><input type="text" placeholder="keyName" name="keyName" id="group_keyname" value="<?=$groupInfo['keyName'];?>"/></div>
        </div>
        <div class="row">
            <div class="col4"><label for="group_description">Description</label></div>
            <div class="col8"><input type="text" placeholder="Short description" name="description" id="group_description" value="<?=$groupInfo['description'];?>"/></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Save"/></div>
        </div>
    </fieldset>
</form>