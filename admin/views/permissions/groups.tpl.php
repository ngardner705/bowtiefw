<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/permissions/group">Add Group</a></li>
    </ul>
</div>

<h1>Groups</h1>
<?php foreach($groupList as $aGroup) { ?>
<div class="row hover-highlight">
    
    <div class="col1"><div class="pad10">
        <?=$aGroup['id'];?>
    </div></div>
    <div class="col3"><div class="pad10">
        <?=$aGroup['title'];?> (<?=$aGroup['keyName'];?>)
    </div></div>
    <div class="col7"><div class="pad10">
        <?=$aGroup['description'];?>
    </div></div>
    <div class="col1"><div class="pad10">
        <?php if($aGroup['id'] > 3) { ?>
        <a href="<?=$DIR_INSTALL;?>admin/permissions/group?group_id=<?=$aGroup['id'];?>" class="miniButton">Edit</a><br/>
        <a href="<?=$DIR_INSTALL;?>admin/permissions/deletegroup?group_id=<?=$aGroup['id'];?>" class="miniButton">Delete</a><br/>
        <?php } ?>
    </div></div>
    
</div>
<?php } ?>