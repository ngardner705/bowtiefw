<h1>Users Permissions</h1>
<p>Manage a users permission groups</p>

<form method="post" action="">
    <input type="hidden" name="savepermissions" value="1"/>
    <div class="row">
        <div class="col1"><div class="pad10">
            <img src="<?=$DIR_INSTALL.$userDetails['avatar'];?>" class="circle"/>
        </div></div>
        <div class="col4"><div class="pad10">
            <?php if(!empty($userDetails['firstname']) || !empty($userDetails['lastname'])) { ?>
            <div class="row"><div class="col12"><?=$userDetails['firstname'];?> <?=$userDetails['lastname'];?></div></div>
            <?php } ?>
            <div class="row"><div class="col12"><?=$userDetails['email'];?></div></div>
            <div class="row"><div class="col12"><?=$userDetails['company'];?> <?=$userDetails['title'];?></div></div>
        </div></div>
        <div class="col5"><div class="pad10">
            <?php foreach($permissionGroups as $aGroup) { ?>
                <div class="hover-highlight">
                    <input type="checkbox" name="grantgroup[]" value="<?=$aGroup['id'];?>" <?=($aGroup['isMember']==1)?'checked="checked"':'';?>/> <?=$aGroup['title'];?>
                </div>
            <?php } ?>
        </div></div>
        <div class="col2"><div class="pad10">
            <input type="submit" value="Save Permissions"/>
        </div></div>
    </div>
</form>