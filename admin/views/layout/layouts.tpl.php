<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/layout/new">Create New Layout</a></li>
    </ul>
</div>

<h1>Layouts</h1>

<?php foreach($layouts as $aLayout) { ?>
<div class="row hover-highlight">
    
    <div class="col2"><div class="pad10">
        <h5><?=$aLayout['title'];?></h5>
    </div></div>
    
    <div class="col6"><div class="pad10">
        <p><?=$aLayout['description'];?></p>
    </div></div>
    
    <div class="col1"><div class="pad10">
        <a href="<?=$DIR_INSTALL;?>admin/layout/edit?layout_id=<?=$aLayout['id'];?>" class="miniButton">Edit</a><br/>
        <a href="<?=$DIR_INSTALL;?>admin/layout/delete?layout_id=<?=$aLayout['id'];?>" class="miniButton">Delete</a><br/>
    </div></div>
    
</div>
<?php } ?>