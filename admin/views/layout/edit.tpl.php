<h1>Layout</h1>

<form method="post" action="<?=$DIR_INSTALL;?>admin/layout/edit">
<input type="hidden" name="dosave" value="1"/>
<input type="hidden" name="layout[id]" value="<?=$layout['id'];?>"/>
<fieldset>
    <legend>Layout Info</legend>
    <div class="field">
        <label for="layout_title">Title</label>
        <input type="text" name="layout[title]" id="layout_title" placeholder="New Layout" value="<?=$layout['title'];?>"/>
    </div>
    <div class="field">
        <label for="layout_description">Description</label>
        <input type="text" name="layout[description]" id="layout_description" placeholder="Description for this layout" value="<?=$layout['description'];?>"/>
    </div>
</fieldset>
<hr/>
<fieldset>
    <legend>Layout Assets</legend>
    
    <h4>&lt;HEAD&gt; assets</h4>
    <?php foreach($layout['layoutAssets'] as $layoutAsset) { if($layoutAsset['location'] == 'header') { ?>
    <div class="row page hover-highlight">
        <div class="col2"><div class="pad10"><div class="pill"><?=$layoutAsset['type'];?></div></div></div>
        <div class="col3"><div class="pad10"><?=$layoutAsset['title'];?></div></div>
        <div class="col6"><div class="pad10"><a href="<?=$layoutAsset['asset'];?>" target="_blank"><?=$layoutAsset['asset'];?></a></div></div>
        <div class="col1"><div class="pad10"><a href="" class="mini-button">delete</a></div></div>
    </div>
    <?php } } ?>
    
    
    <hr/>
    
    <h4>Before &lt;/BODY&gt; assets</h4>
    
    
</fieldset>
<hr/>
<fieldset>
    <legend>Layout Code</legend>
    <div class="field">
        <label for="layout_content">Body</label>
        <textarea name="layout[content]" id="layout_content"><?=htmlentities($layout['content']);?></textarea>
    </div>
</fieldset>
<input type="submit" value="Save"/>
</form>

<!-- add JS popup -->
<div id="addJsAssetPopup" title="Add JavaScript" style="display: none;">
    <form method="post" action="<?=$DIR_INSTALL;?>admin/layout/ajaxAddJsAsset" id="addJsAssetForm">
        <input type="hidden" name="newasset[layout_id]" value="<?=$layout['id'];?>"/>
        <input type="hidden" name="newasset[type]" value="javascript"/>
        <div class="field">
            <label for="newasset_title">Title</label>
            <input type="text" name="newasset[title]" id="newasset_title" placeholder="Title for this asset"/>
        </div>
        <div class="field">
            <label for="newasset_asset">Resource</label>
            <input type="text" name="newasset[asset]" id="newasset_asset" placeholder="URL to asset resource"/>
        </div>
        <div class="field">
            <label for="newasset_location">Location</label>
            <select name="newasset[location]" id="newasset_location"><option value="header">&lt;head&gt;</option><option value="footer">before &lt;/body&gt;</option></select>
        </div>
        <input type="submit" value="Add Asset"/>
    </form>
</div>