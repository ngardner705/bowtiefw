<?php
if(!empty($messages)) {
    echo '<ul id="site-messages">';
    foreach($messages as $message) {
        echo '<li class="'.$message['type'].'">'.$message['message'].'</li>';
    }
    echo '</ul>';
}
?>