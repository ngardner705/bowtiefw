<h1>Log</h1>
<p>Last 100 log entrys</p>

<div class="row">
    <div class="col8"><div class="pad10"><strong>Message</strong></div></div>
    <div class="col2"><div class="pad10"><strong>User</strong></div></div>
    <div class="col2"><div class="pad10"><strong>Date</strong></div></div>
</div>
<?php foreach($recentLogs as $aLog) { ?>
<div class="row hover-highlight">
    <div class="col8"><div class="pad10"><?=$aLog['message'];?></div></div>
    <div class="col2"><div class="pad10"><?=!empty($aLog['user']['email'])?$aLog['user']['email']:'anonymous';?></div></div>
    <div class="col2"><div class="pad10"><?=date("F jS Y g:ia",strtotime($aLog['cdate']));?></div></div>
</div>
<?php } ?>