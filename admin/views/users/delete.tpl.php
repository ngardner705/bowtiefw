<h1>Delete User</h1>
<form method="POST" action="<?=$DIR_INSTALL;?>admin/users/delete">
    <input type="hidden" name="confirmdelete" value="1"/>
    <input type="hidden" name="user_id" value="<?=$userDetails['id'];?>"/>
        <div class="row">
            <div class="col12"><p>Are you sure you want to delete this user? User will be permemantly deleted, you cannot undo this.</p></div>
        </div>
        <div class="row">
            <div class="col4"><label>Email</label></div>
            <div class="col8"><?=$userDetails['email'];?></div>
        </div>
        <div class="row">
            <div class="col4"><label>Username</label></div>
            <div class="col8"><?=$userDetails['username'];?></div>
        </div>
        <div class="row">
            <div class="col4"><label>Joined</label></div>
            <div class="col8"><?=date("F jS Y",strtotime($userDetails['cdate']));?></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Delete"/></div>
        </div>
</form>