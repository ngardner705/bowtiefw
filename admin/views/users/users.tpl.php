<h1>Users</h1>
<?php foreach($userList as $aUser) { ?>
<div class="row hover-highlight">
    
    <div class="col3"><div class="pad10">
        <div class="row"><div class="col12"><?=$aUser['username'];?></div></div>
        <div class="row"><div class="col12"><?=$aUser['email'];?></div></div>
    </div></div>
    <div class="col5"><div class="pad10">
        <?php foreach($aUser['groups'] as $aGroup) { ?>
        <div class="pill"><?=$aGroup['title'];?></div>
        <?php } ?>
    </div></div>
    <div class="col3"><div class="pad10">
        <div class="row"><div class="col12">Joined: <?=date("F jS Y",strtotime($aUser['cdate']));?></div></div>
        <div class="row"><div class="col12">Logged In: <?=date("F jS Y",strtotime($aUser['lastLogin']));?></div></div>
    </div></div>
    <div class="col1"><div class="pad10">
        <a href="<?=$DIR_INSTALL;?>admin/users/edit?user_id=<?=$aUser['id'];?>" class="miniButton">Edit</a><br/>
        <a href="<?=$DIR_INSTALL;?>admin/permissions/user?user_id=<?=$aUser['id'];?>" class="miniButton">Permissions</a><br/>
        <a href="<?=$DIR_INSTALL;?>admin/users/delete?user_id=<?=$aUser['id'];?>" class="miniButton">Delete</a><br/>
    </div></div>
    
</div>
<?php } ?>