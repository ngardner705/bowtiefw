<h1>User</h1>
<form method="POST" action="<?=$DIR_INSTALL;?>admin/users/edit" enctype="multipart/form-data">
    <input type="hidden" name="saveuser" value="1"/>
    <input type="hidden" name="user_id" value="<?=$userDetails['id'];?>"/>
    <fieldset><legend>Information</legend>
        <div class="row">
            <div class="col4"><label for="profile_username">Username</label></div>
            <div class="col8"><?=$userDetails['username'];?></div>
        </div>
        <div class="row">
            <div class="col4"><label for="profile_email">Email</label></div>
            <div class="col8"><input type="text" placeholder="Email" name="email" id="profile_email" value="<?=$userDetails['email'];?>"/></div>
        </div>
        <div class="row">
            <div class="col4"><label>Joined</label></div>
            <div class="col8"><?=date("F jS Y",strtotime($userDetails['cdate']));?></div>
        </div>
        <div class="row">
            <div class="col4"><label>Reset Password</label></div>
            <div class="col8"><a href="<?=$DIR_INSTALL;?>user/forgotpassword?passwordreset=1&email=<?=$userDetails['email'];?>">Send Reset Password Email</a></div>
        </div>
        <div class="row">
            <div class="col12"><input type="submit" value="Save"/></div>
        </div>
    </fieldset>
</form>