<h1>Stylesheets</h1><a class="add-new" href="<?=$DIR_INSTALL;?>admin/css/edit">Add</a>
<?php if(isset($stylesheets)):
    foreach ($stylesheets as $stylesheet): ?>
        <div class="row hover-highlight css-listing">
            <div class="col10">
                <a title="Edit <?=$stylesheet?>" href="<?=$DIR_INSTALL;?>admin/css/edit/<?=$stylesheet?>"><?=$stylesheet?></a>
            </div>
            <div class="col2">
                <a class="trash" href="#">Delete</a>
            </div>
        </div>
    <?php endforeach;
else: ?>
    <p>No stylesheets for this project yet, <a href="#">Create One Now!</a></p>
<?php endif; ?>