<script src="<?=$DIR_INSTALL;?>admin/views/js/codemirror/codemirror.min.js"></script>
<link rel="stylesheet" href="<?=$DIR_INSTALL;?>admin/views/js/codemirror/codemirror.css" />
<?php
$e = false;
if(isset($fileContents)){
    $e = true;
}
?>
<h1><?php echo $e ? 'Edit' : 'New' ?> Stylesheet</h1>
<form id="edit-css" method="POST" action="<?=$DIR_INSTALL;?>admin/css/edit" enctype="multipart/form-data">
    <input type="hidden" name="<?php echo $e ? 'doedit' : 'dosave' ?>" value="1"/>
        <label for="filename">Filename:</lable><input type="text" name="filename" id="filename" <?php echo $e ? 'readonly' : '' ?> value="<?php echo $e ? $fileName : '' ?>" />
    <textarea id="css-editor" name="styles"><?php echo $e ? $fileContents : "/*Write your custom CSS here*/\r\nselector{ style-rule: value; }" ?></textarea>
    <input type="submit" value="Save" />
</form>