<div id="header">
    <div class="container">
	<ul class="utilitynav">
            <li><a href="<?=$DIR_INSTALL;?>user/logout">Logout</a></li>
        </ul>
	<div>
            <div id="logo">
		<a href="<?=$DIR_INSTALL;?>admin">
		    <img src="<?=$DIR_INSTALL;?>common/img/logo.png" alt="<?=$PRODUCT_NAME;?>" title="<?=$PRODUCT_NAME;?>"/>
		    Administration
		</a>
	    </div>
            <ul class="mainnav">
		<li><a href="<?=$DIR_INSTALL;?>admin/pages">Pages</a></li>
		<li><a href="<?=$DIR_INSTALL;?>admin/node/nodes">Content</a><ul>
		    <?php foreach($NODETYPES as $nodeType) { ?>
		    <li><a href="<?=$DIR_INSTALL;?>admin/node/nodes/<?=$nodeType['keyName'];?>"><?=$nodeType['title'];?></a></li>
		    <?php } ?>
		</ul></li>
		<li>Appearance<ul>
		    <li><a href="<?=$DIR_INSTALL;?>admin/layout">Layouts</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/template">Templates</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/css">CSS</a></li>
		</ul></li>
		<li>Administration<ul>
		    <li><a href="<?=$DIR_INSTALL;?>admin/settings">Settings</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/users">Users</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/permissions">Permissions</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/log">Log</a></li>
		    <li><a href="<?=$DIR_INSTALL;?>admin/contentbuilder">Content Builder</a></li>
		</ul></li>
            </ul>
        </div>
    </div>
</div>