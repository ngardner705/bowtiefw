<h1>Creating Page</h1>

<form id="pagePropertiesForm" method="POST" action="<?=$DIR_INSTALL;?>admin/pages/new">
<input type="hidden" name="savepage" value="1"/>
    <fieldset>
        <legend>New Page Properties</legend>
        <div class="field">
            <label for="page_title">Title</label>
            <input type="text" name="page[title]" id="page_title" placeholder="New Page" value=""/>
        </div>
        <div class="field">
            <label for="page_description">Description</label>
            <input type="text" name="page[description]" id="page_description" placeholder="Description of this page" value=""/>
        </div>
        <div class="field">
            <label for="page_parent">Subpage of</label>
            <select name="page[parent_id]" id="page_parent">
                <option value="0">- none -</option>
                <?php foreach($pageList as $page) { ?>
                    <option value="<?$page['id'];?>"><?=$page['title'];?></option>
                <?php } ?>
            </select>
        </div>
        <div class="field">
            <label for="page_url">URL</label>
            <input type="text" name="page[url]" id="page_url" placeholder="URL for this page" value=""/>
        </div>
        <div class="field">
            <label for="page_layout">Layout</label>
            <select name="page[layout_id]" id="page_layout">
                <?php foreach($layoutList as $layout) { ?>
                    <option value="<?=$layout['id'];?>"><?=$layout['title'];?></option>
                <?php } ?>
            </select>
        </div>
    </fieldset>
    <input type="submit" value="Save"/>
</form>