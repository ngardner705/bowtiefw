<h1>Editing Page</h1>

<div id="pageEditor">
    <div class="row">
        <div class="col8">
            <div class="jqueryui-tabs">
                <ul>
                    <li><a href="#preview">Preview</a></li>
                    <li><a href="#properties">Properties</a></li>
                </ul>
                <div id="preview">
                    <div class="previewContainer">
                        <iframe src="<?=$URL.$pageInfo['route']['url'];?>" width="1220" height="600"></iframe>
                    </div>
                </div>
                <div id="properties">
                    <form id="pagePropertiesForm" method="post" action="<?=$URL;?>/admin/pages/edit">
                    <input type="hidden" name="savepage" value="1"/>
                    <input type="hidden" name="id" value="<?=$pageInfo['id'];?>"/>
                    <fieldset>
                        <legend>Page Properties</legend>
                        <div class="field">
                            <label for="page_id">ID:</label>
                            <span id="page_id"><strong><?=$pageInfo['id'];?></strong></span>
                        </div>
                        <div class="field">
                            <label for="page_title">Title</label>
                            <input type="text" name="page[title]" id="page_title" placeholder="New Page" value="<?=$pageInfo['title'];?>"/>
                        </div>
                        <div class="field">
                            <label for="page_description">Description</label>
                            <input type="text" name="page[description]" id="page_description" placeholder="Description of this page" value="<?=$pageInfo['description'];?>"/>
                        </div>
                        <div class="field">
                            <label for="page_parent">Subpage of</label>
                            <select name="page[parent_id]" id="page_parent">
                                <option value="0">- none -</option>
                                <?php foreach($pageList as $page) { ?>
                                    <option value="<?$page['id'];?>" <?=($page['id'] == $pageInfo['parent_id'])?'selected="selected"':'';?>><?=$page['title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="field">
                            <label for="page_url">URL</label>
                            <input type="text" name="page[url]" id="page_url" placeholder="URL for this page" value="<?=$pageInfo['route']['url'];?>"/>
                            <input type="hidden" name="page[route_id]" value="<?=$pageInfo['route']['id'];?>"/>
                        </div>
                        <div class="field">
                            <label for="page_layout">Layout</label>
                            <select name="page[layout_id]" id="page_layout">
                                <?php foreach($layoutList as $layout) { ?>
                                    <option value="<?=$layout['id'];?>" <?=($layout['id'] == $pageInfo['layout_id'])?'selected="selected"':'';?>><?=$layout['title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="field">
                            <label for="page_status">Status</label>
                            <select name="page[status]" id="page_status">
                                <option value="draft" <?=("draft" == $pageInfo['status'])?'selected="selected"':'';?>>Draft</option>
                                <option value="published" <?=("published" == $pageInfo['status'])?'selected="selected"':'';?>>Published</option>
                            </select>
                        </div>
                        <input type="submit" value="Save Properties"/>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col4">
            <div class="jqueryui-tabs">
                <ul>
                    <li><a href="#pageContents">Contents</a></li>
                    <li><a href="#nodes">Nodes</a></li>
                </ul>
                <div id="pageContents">
                    <?php foreach($pagePlaceholders as $placeholderName=>$placeholder) { ?>
                    <div class="placeholder">
                        <fieldset><legend><?=$placeholderName;?></legend>
                        <?php foreach($placeholder['nodeInstances'] as $nodeInstance) { ?>
                        <div class="node"><?=$nodeInstance['node']['title'];?></div>
                        <?php } ?>
                        </fieldset>
                    </div>
                    <?php } ?>
                </div>
                <div id="nodes">
                    <fieldset><legend>Available Nodes</legend>
                    <?php foreach($availableNodes as $availableNode) { ?>
                    <div class="node">
                        <?=$availableNode['title'];?>
                    </div>
                    <?php } ?>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>