<div class="subnav row">
    <ul>
        <li><a href="<?=$DIR_INSTALL;?>admin/pages/new">Create New Page</a></li>
    </ul>
</div>

<h1>Manage Pages</h1>

<div class="pageList">
    <div class="row header">
        <div class="col2"><div class="pad10"><strong>Sort</strong></div></div>
        <div class="col4"><div class="pad10"><strong>Title</strong></div></div>
        <div class="col3"><div class="pad10"><strong>Layout</strong></div></div>
        <div class="col3"><div class="pad10"><strong>Status</strong></div></div>
    </div>
    <?php foreach($contentPages as $contentPage) { ?>
    <div class="row page hover-highlight">
        <div class="col2"><div class="pad10">&#9776;</div></div>
        <div class="col4"><div class="pad10"><a href="<?=$DIR_INSTALL;?>admin/pages/edit?id=<?=$contentPage['id'];?>"><?=$contentPage['title'];?></a></div></div>
        <div class="col3"><div class="pad10"><?=$contentPage['layout']['title'];?></div></div>
        <div class="col3"><div class="pad10"><?=$contentPage['status'];?></div></div>
    </div>
    <?php } ?>
</div>
