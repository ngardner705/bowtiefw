<?php

namespace BowtieFW;

class View {
	
	private $location;
	private $template;
	
	function __construct($location) {
		
		$this->location = $location;
		$this->template_dir = $location.'/views/';
		$this->template = new ViewTemplate;
		$this->assignCommons();
		
	}
	
	function assignCommons() {
		
		$this->assign('PRODUCT_NAME',PRODUCT_NAME);
		$this->assign('URL',URL);
		$this->assign('DIR_INSTALL',INSTALLDIR.'/');
		$this->assign('DIR_TEMPLATE',INSTALLDIR.'/'.$this->template_dir);
		
		// if logged in, assign userInfo
		$objAuth = Authentication::getInstance();
		if($objAuth->loggedIn()) {
			$objUser = new \BowtieFW\Models\User;
			$this->assign('LOGGEDIN',true);
			$this->assign('USERINFO',$objUser->get($objAuth->user_id));
		} else {
			$this->assign('LOGGEDIN',false);
			$this->assign('USERINFO',false);
		}
		
		$permissions = new Permission;
		$this->assign('PERMISSIONS',$permissions->getPermissions());
		
		$objNodeTypes = new \BowtieFW\Models\NodeType;
		$nodeTypeList = $objNodeTypes->getAll();
		$this->assign('NODETYPES',$nodeTypeList); // used in admin to create navigation
		
	}
	
	function assign($variable,$value) {
		
		$this->template->$variable = $value;
		
	}
	
	function findTemplate($template) {
		
		$searchLocation = DIR.$this->template_dir.$template;
		
		if(file_exists($searchLocation) && is_file($searchLocation)) {
			return $searchLocation;
		} else {
			throw new \Exception("Template missing (".DIR.$this->template_dir.$template.")");
		}
		
	}
	
	function fetch($template) {
		
		$templateFilepath = $this->findTemplate($template);
		return $this->template->render($templateFilepath);
		
	}
	
	function display($template) {
		
		$output = $this->fetch($template);
		echo $output;
		
	}
	
	function buildHtmlPage($pageInfo) {
		
		$objAssets = new \BowtieFW\Models\LayoutAssets;
		$coreLayoutAssets = $objAssets->find(array('layout_id'=>0));
		$layoutAssets = $objAssets->find(array('layout_id'=>$pageInfo['layout_id']));
		$layoutAssets = array_merge($coreLayoutAssets,$layoutAssets);
		
		$objMeta = new \BowtieFW\Models\Meta;
		$metaTags = $objMeta->find(array('route_id'=>$pageInfo['route_id']));
		
		$htmlDocument = "<!DOCTYPE html>
<html>
	<head>
		<title>".$pageInfo['title']."</title>
		<script type=\"text/javascript\">var baseURL = '".URL."';</script>
		";
		foreach($metaTags as $metaTag) {
			$htmlDocument .= '<meta name="'.$metaTag['key'].'" content="'.$metaTag['value'].'"/>'."\r\n\t\t";
		}
		foreach($layoutAssets as $layoutAsset) {
			if($layoutAsset['location'] == 'header') {
				if($layoutAsset['type'] == 'css') {
					$htmlDocument .= '<link rel="stylesheet" type="text/css" href="'.$layoutAsset['asset'].'" media="'.$layoutAsset['cssMediaType'].'">'."\r\n\t\t";
				} else if($layoutAsset['type'] == 'javascript') {
					$htmlDocument .= '<script src="'.$layoutAsset['asset'].'"></script>'."\r\n\t\t";
				}
			}
		}
		$htmlDocument .= "
	</head>
	<body>
		";
		
		$htmlDocument .= $this->template->parse($pageInfo['layout']['content']);
		
		foreach($layoutAssets as $layoutAsset) {
			if($layoutAsset['location'] == 'footer') {
				if($layoutAsset['type'] == 'javascript') {
					$htmlDocument .= "\t\t".'<script src="'.$layoutAsset['asset'].'"></script>'."\r\n";
				}
			}
		}
		$htmlDocument .="
	</body>
</html>";
		
		$tagsToRender = $this->findBowtieTags($htmlDocument);
		
		foreach($tagsToRender as $tagToRender) {
			
			$renderedTag = $this->renderTag($tagToRender,$pageInfo);
			$htmlDocument = str_replace($tagToRender['_lookup'],$renderedTag,$htmlDocument);
		}
		
		return $htmlDocument;
		
	}
	
	function findBowtieTags($htmlDoc) {
		
		$numbTags = preg_match_all('/{{(.+)}}/',$htmlDoc,$foundTags);
		
		$tagsToRender = array();
		
		if($numbTags > 0) {
			
			foreach($foundTags[1] as $index=>$foundTag) {
				
				$toRender = array();
				$toRender['_lookup'] = $foundTags[0][$index];
				$tagParts = explode(' ',$foundTag,2);
				
				$tagName = $tagParts[0];
				$toRender['tag'] = $tagName;
				
				if(!empty($tagParts[1])) {
					
					$toRender['params'] = array();
					$tagParamParts = explode(' ',$tagParts[1]);
					
					foreach($tagParamParts as $param) {
						$paramKeyValue = explode('=',$param,2);
						$toRender['params'][$paramKeyValue[0]] = str_replace(array("'",'"'),'',$paramKeyValue[1]);
					}
					
				}
				
				$tagsToRender[] = $toRender;
				
			}
			
		}
		
		return $tagsToRender;
		
	}
	
	function renderTag($tagInfo,$tagData='') {
		
		switch($tagInfo['tag']) {
			
			case 'nodedata':
				
				//$markup = !empty($tagData['node']['nodedata'][$tagInfo['params']['field']])?$tagData['node']['nodedata'][$tagInfo['params']['field']]:'unknown';
				
				$markup = '';
				$hasValues = false;
				
				if(!empty($tagData['node']['nodeFields'])) {
					
					foreach($tagData['node']['nodeFields'] as $nodeField) {
						
						if($nodeField['keyName'] == $tagInfo['params']['field']) {
							
							foreach($nodeField['values'] as $fieldValue) {
								
								$hasValues = true;
								$markup .= $fieldValue['value'];
								
							}
							
							break;
							
						}
						
					}
				}
				
				$tagsToRender = $this->findBowtieTags($markup);
				foreach($tagsToRender as $tagToRender) {
					
					$renderedTag = $this->renderTag($tagToRender);
					$markup = str_replace($tagToRender['_lookup'],$renderedTag,$markup);
					
				}
				
				return $markup;
				
			break;
			
			case 'node':
				
				$nodeObj = new \BowtieFW\Node;
				
				if(!empty($tagInfo['params']['iid'])) {
					
					$nodeInfo = $nodeObj->getNodeInstance($tagInfo['params']['iid']);
					
					if($nodeInfo['ajaxLoad']) {
						$markup = '<div class="bowtie-ajaxContainer" data-nodeiid="'.$tagInfo['params']['iid'].'"></div>';
					} else {
						
						$tagsToRender = $this->findBowtieTags($nodeInfo['template']['content']);
						
						$markup = $nodeInfo['template']['content'];
						
						foreach($tagsToRender as $tagToRender) {
							
							if($tagToRender['tag'] == 'nodedata') {
								$renderedTag = $this->renderTag($tagToRender,$nodeInfo);
							} else {
								$renderedTag = $this->renderTag($tagToRender);
							}
							
							$markup = str_replace($tagToRender['_lookup'],$renderedTag,$markup);
							
						}
						
					}
					
				}
				
				return $markup;
				
			break;
			
			case 'placeholder':
				
				$markup = '<div class="placeholder"></div>';
				
				if(!empty($tagData['id'])) {
					
					$objPageContent = new \BowtieFW\Models\PageContent;
					$pageContents = $objPageContent->find(array('page_id'=>$tagData['id'],'placeholderKey'=>$tagInfo['params']['key']));
					$markup = '<div class="placeholder">';
					foreach($pageContents as $pageContent) { $markup .= '{{node iid='.$pageContent['nodeInstance_id'].'}}'; }
					$markup .= '</div>';
					
				}
				
				$tagsToRender = $this->findBowtieTags($markup);
				foreach($tagsToRender as $tagToRender) {
					
					$renderedTag = $this->renderTag($tagToRender);
					$markup = str_replace($tagToRender['_lookup'],$renderedTag,$markup);
					
				}
				
				return $markup;
				
			break;
		}
		
		return $markup;
		
	}
	
}

class ViewTemplate {
	
	function render($__template) {
		
		extract($this->getVars());
		
		ob_start();
		include $__template;
		return ob_get_clean();
		
	}
	
	function parse($template) {
		
		extract($this->getVars());
		ob_start();
		eval(' ?>'.$template.'<?php ');
		$evaldTemplate = ob_get_clean();
		
		
		
		return $evaldTemplate;
		
	}
	
	function getVars() {
		
		$templateVars = get_object_vars($this);
		return $templateVars;
		
	}
	
}

?>
