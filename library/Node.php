<?php
/**
 * Node
 */

namespace BowtieFW;

class Node {
    
    private $nodeModel;
    private $nodeInstanceModel;
    private $nodeFieldOptions; // temp cache per page load
    
    function __construct() {
        
        $this->nodeModel = new Models\Node;
        $this->nodeInstanceModel = new Models\NodeInstance;
        
    }
    
    public function newNode($nodeType) {
        
        $node = array(
            'id'=>false,
            'title'=>'',
            'description'=>'',
            'status'=>'draft');
        
        $objNodeType = new \BowtieFW\Models\NodeType;
        $nodeTypeInfo = $objNodeType->getFromKeyname($nodeType);
        $node['nodeType'] = $nodeTypeInfo;
        
        $objNodeFields = new \BowtieFW\Models\NodeFieldInstance;
        $nodeFields = $objNodeFields->find(array('nodeType_id'=>$nodeTypeInfo['id']));
        
        foreach($nodeFields as &$nodeField) { $nodeField['values'] = array(); }
        
        $node['nodeFields'] = $nodeFields;
        
        return $node;
        
    }
    
    public function getNode($nodeId) {
        
        $node = $this->nodeModel->getRelatedData($nodeId);
        
        $objNodeFields = new \BowtieFW\Models\NodeFieldInstance;
        
        if($node) {
            
            // get nodes fields
            $nodeFields = $objNodeFields->find(array('nodeType_id'=>$node['nodeType_id']));
            if(!empty($nodeFields)) {
                
                foreach($nodeFields as &$nodeField) {
                    
                    // get values for field
                    $storageTable = $this->getFieldStorageTable($nodeField);
                    $className = '\\BowtieFW\\Models\\'.$storageTable;
                    $objNodeData = new $className;
                    $fieldValues = $objNodeData->find(array('nodeFieldInstance_id'=>$nodeField['id'],'node_id'=>$nodeId));
                    
                    $nodeField['values'] = $fieldValues;
                    
                }
                
            }
            
            $node['nodeFields'] = $nodeFields;
            
        }
        
        return $node;
        
    }
    
    public function getNodeInstance($nodeInstanceId) {
        
        $nodeInstance = $this->nodeInstanceModel->getRelatedData($nodeInstanceId);
        
        if($nodeInstance) {
            $nodeInstance['node'] = $this->getNode($nodeInstance['node_id']);
        }
        
        return $nodeInstance;
        
    }
    
    private function getFieldStorageTable($field) {
        
        if(empty($this->nodeFieldOptions)) {
            $objNodeFieldOptions = new \BowtieFW\Models\NodeFieldOption;
            $this->nodeFieldOptions = $objNodeFieldOptions->getAll();
        }
        
        $storageTable = 'nodeFieldTextValue'; // default
        
        if(!empty($field['fieldType'])) {
            
            foreach($this->nodeFieldOptions as $fieldOption) {
                
                if($fieldOption['type'] == $field['fieldType']) {
                    $storageTable = $fieldOption['storageTable'];
                    break;
                }
                
            }
            
            reset($this->nodeFieldOptions);
            
        }
        
        return $storageTable;
        
    }
    
}

?>