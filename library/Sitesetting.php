<?php
/**
 * Sitesetting
 * 
 */

namespace BowtieFW;

class Sitesetting {
    
    private static $instance;
    private $settings;
    
    function __construct() {
        
        $this->sitesettingModel = new Models\Sitesetting;
        $this->loadSettings();
        self::$instance = $this;
        
    }
    
    static function getInstance() {
        
        if (!isset(self::$instance)) {
            
            $c = __CLASS__;
            self::$instance = new $c;
            
        }
        
        return self::$instance;
        
    }
    
    function loadSettings() {
        
        $records = $this->sitesettingModel->getAll();
        
        if(is_array($records)) {
            
            foreach($records as $record) {
                
                $this->setEntry($record['group'],$record['key'],$record['value'],$record['title'],$record['options'],$record['description'],$record['id']);
                
            }
            
            return true;
            
        } else {
            
            return false;
            
        }
        
    }
    
    private function setEntry($group,$name,$value,$title,$options = null,$description,$id) {
        
        if(empty($this->settings[$group])) {
            
            $this->settings[$group] = array();
            
        }
        $this->settings[$group][$name]['group'] = $group;
        $this->settings[$group][$name]['title'] = $title;
        $this->settings[$group][$name]['key'] = $name;
        $this->settings[$group][$name]['value'] = $value;
        $this->settings[$group][$name]['description'] = $description;
        if($options){
            $this->settings[$group][$name]['options'] = $options;
        }
        $this->settings[$group][$name]['id'] = $id;
    }
    
    function getEntry($group,$name) {
        
        if(isset($this->settings[$group][$name])) {
            
            return $this->settings[$group][$name];
            
        } else {
            
            return false;
            
        }
        
    }

    function getEntries(){
        
        if(is_array($this->settings)){
            return $this->settings;
        }else{
            return false;
        }
        
    }
    
}

?>