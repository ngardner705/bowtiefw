<?php

namespace BowtieFW;

require_once(DIR.'library/PHPMailer/class.phpmailer.php');
require_once(DIR.'library/PHPMailer/class.pop3.php');
require_once(DIR.'library/PHPMailer/class.smtp.php');

class Email {
    
    public $error;
    
    function setupSMTP(&$mail) {
        
        $mail->isSMTP();
        $mail->Host = EMAIL_SMTP_HOST;
        $mail->Port = EMAIL_SMTP_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SMTP_USERNAME;
        $mail->Password = EMAIL_SMTP_PASSWORD;
        //$mail->SMTPDebug = 2;
        
    }
    
    /**
     * Sends the password reset email
     */
    function passwordForgot($email,$tempCode) {
        
        $mail = new \PHPMailer();
        $this->setupSMTP($mail);
        
        $mail->setFrom(EMAIL_FROM, EMAIL_FROMNAME);
        $mail->addAddress($email);
        $mail->Subject = PRODUCT_NAME.' | Forgotten password';
        $mail->Body = 'To reset your password, goto <a href="'.URL.'user/recover?a='.$email.'&b='.$tempCode.'">'.URL.'user/recover?a='.$email.'&b='.$tempCode.'</a>';
        $mail->AltBody = 'To reset your password, goto '.URL.'user/recover?a='.$email.'&b='.$tempCode;
        
        //send the message, check for errors
        if (!$mail->send()) {
            $this->error = $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
        
    }
    
    /**
     * Used for sending 1 time emails
     */
    function genericEmail($to,$subject,$body) {
        
        $mail = new \PHPMailer();
        $this->setupSMTP($mail);
        
        $mail->setFrom(EMAIL_FROM, EMAIL_FROMNAME);
        
        // add To addresses
        if(is_array($to)) {
            foreach($to as $toAddress) {
                $mail->addAddress($toAddress);
            }
        } else {
            $mail->addAddress($to);
        }
        
        // subject and body
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        //send the message, check for errors
        if (!$mail->send()) {
            $this->error = $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
        
    }
    
}

?>