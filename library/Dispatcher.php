<?php
/**
 * This file contains the dispatcher
 */

namespace BowtieFW;

class Dispatcher {
	
	var $context;
	var $controller;
	var $action;
	var $params;
	private $urlRequest;
	
	function __construct() {
		
		$this->setContext();
		$this->setController();
		$this->setAction();
		$this->setParams(array());
		$this->urlRequest = '';
		
	}
	
	function setContext($context='frontend') {
		
		$context = strtolower($context);
		$this->context = $context;
		
	}
	
	function setAction($action='index') {
		
		$action = strtolower($action);
		$this->action = $action;
		
	}
	
	function setController($controller='page') {
		
		$controller = strtolower($controller);
		$this->controller = $controller;
		
	}
	
	function setParams(array $params) {
		
		$cleanParams = array();
		
		foreach($params as $key=>$value) {
			
			if(is_string($value)) {
				
				$value = htmlspecialchars($value);
				
			}
			
			$cleanParams[$key] = $value;
			
		}
		
		$this->params = $cleanParams;
		
	}
	
	private function runAction() {
		
		$objLog = new Models\Log;
		
		try {
			
			$className = ucfirst(strtolower($this->controller));
			$controllerClass = '\\BowtieFW\\'.ucfirst(strtolower($this->context)).'\\'.$className;
			
			// autoload the controllers
			$includeFile = DIR.$this->context.'/controllers/'.$className.'.php';
			if(file_exists($includeFile)) {
				include_once $includeFile;
			}
			
			if(class_exists($controllerClass)) {
				
				$objController = new $controllerClass;
				
			} else {
				
				$this->error404('Class "'.$controllerClass.'" does not exist');
				
			}
			
			if ($objController instanceof Controller) {
				
				if(method_exists($objController,'action'.$this->action)) {
					
					// Permission check
					$objPerm = new Permission;
					if($objPerm->checkPermission($this->controller,$this->action,$this->context)) {
						
						$objLog->Log("Execute : ".$this->context.": ".$this->controller."->".$this->action);
						$objController->setPlace($this->context);
						$objController->setLayout();
						$objController->execute('action'.$this->action,$this->params);
						
					} else {
						
						$this->error403("Permission Denied");
						
					}
					
				} else {
					
					$this->error404("Action ".$this->action." does not exist for ".$this->controller);
					
				}
				
			} else {
				
				$this->error500('Class '.$className.' does not extend Controller class!');
				
			}
			
		} catch(\Exception $e) {
			
			// bubble up
			throw new \Exception($e->getMessage());
			
		}
		
	}
	
	function dispatch() {
		
		try {
			
			$this->runAction();
			
		} catch(\Exception $e) {
			
			$this->error500($e->getMessage());
			
		}
		
	}
	
	function parseUrl($urlString) {
		
		global $params;
		$this->urlRequest = $urlString;
		
		// no URL, goto homepage
		if(empty($urlString)) {
			
			// nothing passed, default to homepage
			$this->setContext('frontend');
			$this->setController('page');
			$this->setAction('index');
			$this->setParams($params);
			return true;
			
		}
		
		// lookup route
		$objRoute = new Models\Route;
		$route = $objRoute->find(array("url"=>$urlString));
		if(!empty($route)) { $foundRoute = $route[0]; }
		
		if(!empty($foundRoute)) {
			
			// based on found route set context, controller, and action
			$this->setContext($foundRoute['context']);
			$this->setController($foundRoute['controller']);
			$this->setAction($foundRoute['action']);
			
			// merge params together and set
			$routeParams = unserialize($foundRoute['params']);
			if(is_array($routeParams)) { $params = array_merge($params,$routeParams); }
			$this->setParams($params);
			
		} else {
			
			// url not found, lets try controller/action logic
			$urlparts = explode('/',$urlString);
			if($urlparts[0] == 'admin') {
				
				// admin controller/action
				$this->setContext('admin');
				$this->setController(!empty($urlparts[1])?$urlparts[1]:'page');
				$this->setAction(!empty($urlparts[2])?$urlparts[2]:'index');
				$params['_extra'] = array_slice($urlparts,3);
				$this->setParams($params);
				
			} else {
				
				// frontend controller/action
				$this->setContext('frontend');
				$this->setController($urlparts[0]);
				$this->setAction(!empty($urlparts[1])?$urlparts[1]:'index');
				$params['_extra'] = array_slice($urlparts,2);
				$this->setParams($params);
				
			}
			
		}
		
		return true;
		
	}
	
	function error403($error='') {
		
		header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden',true,403);
		
		$objLog = new Models\Log;
		$objLog->Log("403 : ".$this->context." : ".$this->controller."->".$this->action);
		
		echo '
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<title>'.PRODUCT_NAME.' 403</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="stylesheet" href="'.URL.'common/css/reset.min.css">
			<link rel="stylesheet" href="'.URL.'frontend/views/css/style.css">
		</head>
		<body>
		
		<div class="container">
			<div style="text-align: center;"><a href="'.URL.'"><img src="'.URL.'common/img/logo.png" alt="'.PRODUCT_NAME.'" title="'.PRODUCT_NAME.'"/></a></div>
			<h2>403</h2>
			<p>Forbidden | '.$this->context.' : '.$this->controller.'->'.$this->action.'</p>
			<hr/>
			<strong>'.htmlentities($error).'</strong><br/>
		</div>
		
		</body>
		</html>
		';
		exit(0);
		
	}
	
	function error404($error='') {
		
		header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found',true,404);
		
		$objLog = new Models\Log;
		$objLog->Log("404 : ".$this->urlRequest." | ".$this->controller."->".$this->action);
		
		echo '
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<title>'.PRODUCT_NAME.' 404</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="stylesheet" href="'.URL.'common/css/reset.min.css">
			<link rel="stylesheet" href="'.URL.'frontend/views/css/style.css">
		</head>
		<body>
		<div class="container">
			<div style="text-align: center;"><a href="'.URL.'"><img src="'.URL.'common/img/logo.png" alt="'.PRODUCT_NAME.'" title="'.PRODUCT_NAME.'"/></a></div>
			<h2>404</h2>
			<p>Sorry, but the page wasn\'t found</p>
			<hr/>
			<strong>'.htmlentities($error).'</strong><br/>
			<ul>
				<li>Context: '.$this->context.'</li>
				<li>Class: '.$this->controller.'</li>
				<li>Function: '.$this->action.'</li>
			</ul>
		</div>
		
		</body>
		</html>
		';
		exit(0);
		
	}
	
	function error500($error='') {
		
		header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error',true,500);
		
		$objLog = new Models\Log;
		$objLog->Log("500 : ".$this->controller."->".$this->action);
		
		echo '
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<title>'.PRODUCT_NAME.' 500</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="stylesheet" href="'.URL.'common/css/reset.min.css">
			<link rel="stylesheet" href="'.URL.'frontend/views/css/style.css">
		</head> 
		<body>
		
		<div class="container">
			<div style="text-align: center;"><a href="'.URL.'"><img src="'.URL.'common/img/logo.png" alt="'.PRODUCT_NAME.'" title="'.PRODUCT_NAME.'"/></a></div>
			<h2>500</h2>
			<p>Sorry, Internal Server Error</p>
			<hr/>
			<strong>'.htmlentities($error).'</strong><br/>
			<ul>
				<li>Context: '.$this->context.'</li>
				<li>Class: '.$this->controller.'</li>
				<li>Function: '.$this->action.'</li>
			</ul>
		</div>
		
		</body>
		</html>
		';
		exit(0);
		
	}
	
}

?>
