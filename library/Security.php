<?php
/**
 * This Security class is used to encrypt and decrypt data
 * for secure storage. Also has hashing methods for passwords.
 *
 * NOTE: If you use the users un/pw to generate their key, know
 * that if they change their un or pw they will be unable to
 * decrypt old data. All their data would need to be reencrypted
 * during a password change to prevent this.
 * 
 * How to use
 * $this->generateKey()
 * Generate a DATA KEY, usually with the users un and password
 *
 * $this->storeDataKey()
 * Then encrypt the users DATA KEY, and stores the encrypted
 * version and the IV in their cookies. Stores the key to
 * decrypt their DATA KEY on our server in a session.
 *
 * $this->getDataKey()
 * When you need to encrypt or decrypt data, you need use the
 * clients DATA KEY. This uses their cookies and our session
 * to decrypt their DATA KEY.
 *
 * $this->encrypt()
 * Encrypt data
 *
 * $this->decrypt()
 * Decrypt data
 */

namespace BowtieFW;

class Security {
    
    function __construct() { }
    
    /**
     * Generates a KEY from 2 strings
     */
    public static function generateKey($part1,$part2) {
        
        $secretSalt = 'm22Q&^P<I3';
        
        // salt|part1|salt|part2|salt
        $dataKey  = $secretSalt.'|';
        $dataKey .= hash('sha256',$part1).'|';
        $dataKey .= $secretSalt.'|';
        $dataKey .= hash('sha256',$part2).'|';
        $dataKey .= $secretSalt;
        
        $dataKey = hash('sha256',$dataKey,true);
        
        return $dataKey;
        
    }
    
    /**
     * Gets the clients KEY
     * We do this by getting their iv and encrypted key, and use our key to decode their key
     */
    public static function getDatakey() {
        
        $key_iv = !empty($_COOKIE['datakey_iv'])?base64_decode($_COOKIE['datakey_iv']):false; // client side
        $key_data = !empty($_COOKIE['datakey_encrypted'])?$_COOKIE['datakey_encrypted']:false; // client side
        $key_key = !empty($_SESSION['datakey_key'])?$_SESSION['datakey_key']:false; // server side
        
        if(!empty($key_iv) && !empty($key_data) && !empty($key_key)) {
            
            $datakey = Security::decrypt($key_key,$key_data,$key_iv);
            return $datakey;
            
        } else {
            
            throw new \Exception("Datakey missing!");
            
        }
        
    }
    
    /**
     * Encrypt the clients KEY, then stored the encrypted key and IV on their computer
     * Also store the key to decrypt their key on our server
     */
    public static function storeDatakey($un,$pw) {
        
        // get the KEY which is used to encrypt and decrypt their DATA - this is the DataKey
        $secret = Security::generateKey($un,$pw);
        
        // geneate another random KEY which is used to encrypt the DATAS KEY
        $keyPart1 = microtime(true);
        $keyPart2 = $un;
        $secretKey = Security::generateKey($keyPart1,$keyPart2);
        
        // encrypt the DATAs KEY for storage on client
        $iv = Security::generateIV();
        $encrypted = Security::encrypt($secretKey,$secret,$iv);
        
        // save the encrypted DATAs KEY on the client
        setcookie('datakey_iv', base64_encode($iv), 0, '/', null, false, true);
        setcookie('datakey_encrypted', $encrypted, 0, '/', null, false, true);
        
        // save the DATAs KEY's KEY on server
        $_SESSION['datakey_key'] = $secretKey;
        
    }
    
    /**
     * Hashing function for passwords
     */
    public static function hashPassword($password) {
        $hashed = hash('sha256','Salt_'.$password.'_Pepper');
        return $hashed;
    }
    
    /**
     * Genreates an IV for encrpytion
     */
    public static function generateIv() {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        return mcrypt_create_iv($iv_size, MCRYPT_RAND);
    }
    
    /**
     * Encypts data
     */
    public static function encrypt($key, $data, $iv) {
        $ret = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_CBC, $iv);
        return base64_encode($ret);
    }
    
    /**
     * Decrypts data
     */
    public static function decrypt($key, $data, $iv) {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($data), MCRYPT_MODE_CBC, $iv), "\0");
    }

}
?>