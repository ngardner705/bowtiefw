<?php

namespace BowtieFW\Models;

class Sitesetting extends \BowtieFW\Model {
    
    private $settings;
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'siteSetting';
        $this->install();
        
    }
    
    private function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE `siteSetting` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `key` varchar(45) NOT NULL,
                `value` varchar(255) NOT NULL,
                `options` varchar(255) DEFAULT '',
                `group` varchar(45) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->db->query($sql);
        }
        
        // create default settings
        $this->save(array(
            'title'=>'Homepage ID',
            'description'=>'page ID of homepage',
            'key'=>'homepage',
            'value'=>'1',
            'group'=>'CMS Settings'
        ));
        
    }

    function updateValue($key, $value){

        //update siteSetting values
        $sql = "
        UPDATE `siteSetting`
        SET `value` = '$value'
        WHERE `key` = '$key'";
        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }

    }

    function updateSetting($data){

        //update siteSetting data
        $lookupKey = $data['key'];
        unset($data['key']);
        $ts = date('Y-m-d H:i:s');
        $sql = "UPDATE `siteSetting` SET ";
        $parts = array();
        foreach ($data as $key => $value) {
            $parts[] = "`".$key."` = '".$value."'";
        }
        $sql = $sql.implode(",", $parts).", WHERE `key` = '".$lookupKey."'";
        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }

    }

    function keyUnique($key){

        $sql = "SELECT * FROM `siteSetting` WHERE `key` = '$key'";
        $result = $this->db->getAll($sql);
        if(!empty($result)){
            return false;
        }else{
            return true;
        }

    }
    
}
?>