<?php

namespace BowtieFW\Models;

class Page extends \BowtieFW\Model {
    
    public $pageContent;
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'page';
        $this->install();
        
        $this->pageContent = new PageContent;
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `page` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `content` text NOT NULL,
                `layout_id` int(11) NOT NULL,
                `status` varchar(45) NOT NULL,
                `parent_id` int(11) NOT NULL,
                `route_id` int(11) NOT NULL,
                `sortOrder` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
            ";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class PageContent extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'pageContent';
        $this->install();
        
    }
    
    function install() {
        
        if($this->tableExists()) {
            
        } else {
            // install it
            $sql = "
            CREATE TABLE `pageContent` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `page_id` int(11) NOT NULL,
                `placeholderKey` varchar(45) NOT NULL,
                `nodeInstance_id` int(11) NOT NULL,
                `sortOrder` int(11) NOT NULL,
                `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
            ";
            
            $this->db->query($sql);
        }
        
    }
    
}

?>