<?php

namespace BowtieFW\Models;

class Search extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'search';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `search` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `searchQuery` varchar(255) NOT NULL,
                `user_id` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

?>