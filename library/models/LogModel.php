<?php

namespace BowtieFW\Models;

class Log extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'log';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE
                `log` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `message` text COLLATE utf8_unicode_ci NOT NULL,
                    `user_id` int(11) NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
        }
        
    }
    
    function getRecent($howmany = 10) {
        
        $sql = "SELECT * FROM `".$this->tableName."` ORDER BY `cdate` DESC,`id` DESC LIMIT :howmany";
        $results = $this->db->getAll($sql,array('howmany'=>$howmany));
        
        return $results;
        
    }
    
    function Log($message) {
        
        $objAuth = \BowtieFW\Authentication::getInstance();
        
        $saveData = array(
            'message'=>$message,
            'user_id'=>intval($objAuth->user_id)
        );
        
        $savedId = $this->save($saveData);
        
        if($savedId) {
            return true;
        } else {
            return false;
        }
        
    }
    
}
?>