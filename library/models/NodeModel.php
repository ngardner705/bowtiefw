<?php

namespace BowtieFW\Models;

class Node extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'node';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `node` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `nodeType_id` int(11) NOT NULL,
                `user_id` int(11) NOT NULL COMMENT 'who created this node',
                `status` varchar(45) NOT NULL,
                `parent_id` int(11) NOT NULL,
                `route_id` int(11) NOT NULL,
                `searchable` tinyint(1) NOT NULL DEFAULT '1',
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class NodeFieldInstance extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldInstance';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldInstance` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `nodeType_id` int(11) NOT NULL,
                `fieldType` varchar(45) NOT NULL,
                `keyName` varchar(45) NOT NULL,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `required` tinyint(1) NOT NULL DEFAULT '0',
                `default` VARCHAR(45) NOT NULL,
                `options` VARCHAR(255) NOT NULL,
                `numbAllowedValues` INT NOT NULL DEFAULT  '1' COMMENT  '0 = unlimited',
                `sortOrder` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class NodeFieldOption extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldOption';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldOption` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `type` varchar(45) NOT NULL COMMENT 'text, wysiwyg, image, file',
                `description` varchar(255) NOT NULL,
                `storageTable` varchar(255) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
            // install field types
            new NodeFieldIntValue;
            new NodeFieldDateValue;
            new NodeFieldNumberValue;
            new NodeFieldTextValue;
            new NodeFieldVarcharValue;
            
        }
        
    }
    
}

class NodeInstance extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeInstance';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeInstance` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `template_id` int(11) NOT NULL,
                `ajaxLoad` tinyint(1) NOT NULL DEFAULT '0',
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class NodeInstanceDevice extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeInstanceDevice';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeInstanceDevice` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `nodeInstance_id` int(11) NOT NULL,
                `deviceGroup` varchar(45) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class NodeType extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeType';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeType` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `keyName` varchar(45) NOT NULL,
                `template` varchar(255) NOT NULL,
                `group` varchar(45) NOT NULL,
                `parent_id` int(11) NOT NULL,
                `sortOrder` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
        }
        
    }
    
    function getFromKeyname($nodeTypeKey='') {
        
        if($nodeTypeKey) {
            $nodeTypeInfo = $this->find(array('keyName'=>$nodeTypeKey));
            if($nodeTypeInfo) {
                $nodeTypeInfo = $nodeTypeInfo[0];
            } else {
                $nodeTypeInfo = false; // nodeType not found
            }
        } else {
            $nodeTypeInfo = array(
                'id'=>0,
                'title'=>'All',
                'keyName'=>'',
                'template'=>'',
                'group'=>'',
                'parent_id'=>0,
                'sortOrder'=>0
            );
        }
        
        return $nodeTypeInfo;
        
    }
    
}

class NodeFieldIntValue extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldIntValue';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldIntValue`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `nodeFieldInstance_id` int(11) NOT NULL,
                `value` int(11) NOT NULL,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
            // add it as option to node field types
            $nodeFieldOption = new NodeFieldOption;
            $nodeFieldOption->save(array(
                'title'=>'Integer',
                'type'=>'integer',
                'description'=>'int field',
                'storageTable'=>'nodeFieldIntValue'
            ));
            
        }
        
    }
    
}

class NodeFieldTextValue extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldTextValue';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldTextValue`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `nodeFieldInstance_id` int(11) NOT NULL,
                `value` longtext NOT NULL,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
            // add it as option to node field types
            $nodeFieldOption = new NodeFieldOption;
            $nodeFieldOption->save(array(
                'title'=>'Text',
                'type'=>'longtext',
                'description'=>'long text field',
                'storageTable'=>'nodeFieldTextValue'
            ));
            
        }
        
    }
    
}

class NodeFieldVarcharValue extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldVarcharValue';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldVarcharValue`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `nodeFieldInstance_id` int(11) NOT NULL,
                `value` varchar(255) NOT NULL,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
        }
        
        // add it as option to node field types
        $nodeFieldOption = new NodeFieldOption;
        $nodeFieldOption->save(array(
            'title'=>'String',
            'type'=>'varchar',
            'description'=>'text field, up to 255 characters',
            'storageTable'=>'nodeFieldVarcharValue'
        ));
        
    }
    
}

class NodeFieldDateValue extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldDateValue';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldDateValue`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `nodeFieldInstance_id` int(11) NOT NULL,
                `value` datetime NOT NULL,
                `mdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
            // add it as option to node field types
            $nodeFieldOption = new NodeFieldOption;
            $nodeFieldOption->save(array(
                'title'=>'Date and Time',
                'type'=>'datetime',
                'description'=>'datetime field',
                'storageTable'=>'nodeFieldDateValue'
            ));
            
        }
        
    }
    
}

class NodeFieldNumberValue extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'nodeFieldNumberValue';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `nodeFieldNumberValue`(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `node_id` int(11) NOT NULL,
                `nodeFieldInstance_id` int(11) NOT NULL,
                `value` decimal(30,6) NOT NULL,
                `mdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            
            $this->db->query($sql);
            
            // add it as option to node field types
            $nodeFieldOption = new NodeFieldOption;
            $nodeFieldOption->save(array(
                'title'=>'Number',
                'type'=>'decimal',
                'description'=>'decimal field, 30 characters with precision of 6',
                'storageTable'=>'nodeFieldNumberValue'
            ));
            
        }
        
    }
    
}

?>