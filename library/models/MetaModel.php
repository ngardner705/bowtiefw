<?php

namespace BowtieFW\Models;

class Meta extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'meta';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `meta` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `key` varchar(45) NOT NULL,
                `value` varchar(255) NOT NULL,
                `route_id` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class MetaOptions extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'metaOptions';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `metaOptions` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `key` varchar(45) NOT NULL,
                `sortOrder` int(11) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

?>