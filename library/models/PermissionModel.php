<?php

namespace BowtieFW\Models;

class Permission extends \BowtieFW\Model {
    
    public $groupUsers;
    public $groupPermissions;
    public $userPermissions;
    public $groups;
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'permission';
        
        $this->groups = new PermissionGroup;
        $this->groupUsers = new PermissionGroupUser;
        $this->groupPermissions = new PermissionGrantGroup;
        $this->userPermissions = new PermissionGrantUser;
        
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE
                `".$this->tableName."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `context` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `controller` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `action` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `cca` (`context`,`controller`,`action`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
            $this->generatePermissions();
            $this->generatePermissions('admin');
            
        }
        
    }
    
    /**
     * Gets all the permissions for a user, including
     * their groups permissions
     */
    function getPermissions($user_id=0) {
        
        $permissionList = array();
        
        $user_id = intval($user_id);
        
        // get the users groups
        $groups = $this->groupUsers->getUserGroups($user_id);
        
        // get the groups permissions
        $groupPermissions = array();
        if(!empty($groups)) {
            foreach($groups as $group) {
                $groupPermissions[] = $this->groupPermissions->getPermissions($group['id']);
            }
        }
        
        // get the users permissions
        $userPermissions = $this->userPermissions->getPermissions($user_id);
        
        // merge group and user permissions
        $permissionList = $userPermissions;
        foreach($groupPermissions as $groupPermission) {
            $permissionList = array_merge($permissionList,$groupPermission);
        }
        return $permissionList;
        
    }
    
    /**
     * Returns all defined permissions
     */
    function getAllPermissions() {
        
        $allPermissions = array();
        
        $sql = "
        SELECT
            p.`id`,
            p.`context`,
            p.`controller`,
            p.`action`,
            g.`id` as 'group_id',
            g.`title` as 'group_title'
        FROM
            `permission` as p
        LEFT JOIN `permission_grant_group` as pgg ON
            p.`id` = pgg.`permission_id`
        LEFT JOIN `permission_group` as g ON
            pgg.`permission_group_id` = g.`id`
        ORDER BY
            p.`context`,
            p.`controller`,
            p.`action`
        ";
        
        $permissionsAndGroups = $this->db->getAll($sql);
        
        if(!empty($permissionsAndGroups)) {
            
            foreach($permissionsAndGroups as $permAndGroup) {
                
                if(empty($allPermissions[$permAndGroup['id']])) {
                    
                    // create it
                    $allPermissions[$permAndGroup['id']] = array(
                        'id'=>$permAndGroup['id'],
                        'context'=>$permAndGroup['context'],
                        'controller'=>$permAndGroup['controller'],
                        'action'=>$permAndGroup['action']
                    );
                    
                }
                
                $allPermissions[$permAndGroup['id']]['groups'][] = array('id'=>$permAndGroup['group_id'],'title'=>$permAndGroup['group_title']);
                
            }
            
        }
        
        return $allPermissions;
        
    }
    
    /**
     * Scans the directory for controllsers/actions
     * and creates permissions for new ones
     *
     * Permissions should then be assigned to groups or users
     */
    function generatePermissions($directory='frontend') {
        
        $newPermissions = array();
        $permissionList = array();
        $controllerList = array();
        $scanDir = DIR.$directory.'/controllers/';
        
        $fileList = scandir($scanDir);
        if(!empty($fileList)) {
            foreach($fileList as $file) {
                if(is_file($scanDir.$file)) {
                    $controllerList[] = substr($file,0,-4); // strip off .php
                }
            }
        }
        
        if(!empty($controllerList)) {
            foreach($controllerList as $controller) {
                
                include_once $scanDir.$controller.'.php';
                $controllerClass = '\\BowtieFW\\'.ucfirst(strtolower($directory)).'\\'.$controller;
                $methodList = get_class_methods($controllerClass);
                
                if(!empty($methodList)) {
                    foreach($methodList as $methodName) {
                        if(substr($methodName,0,6) == 'action') {
                            $permissionList[] = array('context'=>strtolower($directory),'controller'=>strtolower($controller),'action'=>strtolower(substr($methodName,6)));
                        }
                    }
                }
                
            }
        }
        
        $currentPermissions = $this->getAllPermissions();
        
        if(!empty($permissionList)) {
            
            if(!empty($currentPermissions)) {
                
                foreach($permissionList as $possiblePermission) {
                    
                    $alreadyExists = false;
                    
                    foreach($currentPermissions as $permission) {
                        
                        if(
                           strtolower($permission['controller']) == strtolower($possiblePermission['controller']) &&
                           strtolower($permission['action']) == strtolower($possiblePermission['action']) &&
                           strtolower($permission['context']) == strtolower($possiblePermission['context'])) {
                            
                            $alreadyExists = true;
                            break;
                            
                        }
                        
                    }
                    
                    if(!$alreadyExists) {
                        
                        $newPermissions[] = $possiblePermission;
                        
                    }
                    
                }
                
            } else {
                
                $newPermissions = $permissionList;
                
            }
            
        } else {
            
            $newPermissions = array();
            
        }
        
        if(!empty($newPermissions)) {
            
            // save the new ones!
            foreach($newPermissions as $newPermission) {
                $this->save($newPermission);
            }
            
        }
        
    }
    
    function setPermissions($permissions) {
        
        // sets all the permissions
        
    }
    
}

class PermissionGrantUser extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'permission_grant_user';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE
                `".$this->tableName."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `permission_id` int(11) NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
        }
        
    }
    
    function getPermissions($user_id) {
        
        $result = $this->db->getAll("SELECT p.`context`, p.`controller`, p.`action` FROM `permission` as p LEFT JOIN `permission_grant_user` as pgu ON pgu.`permission_id` = p.`id` WHERE pgu.`user_id` = :user_id",array("user_id"=>$user_id));
        return $result;
        
    }
    
}

class PermissionGroup extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'permission_group';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE
                `".$this->tableName."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
                    `description` text COLLATE utf8_unicode_ci NOT NULL,
                    `keyName` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `title` (`title`),
                    UNIQUE KEY `keyname` (`keyName`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
            // default groups
            $this->save(array("title"=>"Anonymous","description"=>"All site visitors are anonymous.","keyName"=>"anonymous")); // 1
            $this->save(array("title"=>"Authenticated User","description"=>"Users who have an account and are logged in.","keyName"=>"authenticated")); // 2
            $this->save(array("title"=>"Administrator","description"=>"Site administrators. This is a special group, that always has all permissions.","keyName"=>"administrator")); // 3
        }
        
    }
    
    function deleteGroup($group_id) {
        
        // delete users
        $this->db->query("DELETE FROM `permission_group_user` WHERE `permission_group_id` = :permission_group_id",array("permission_group_id"=>$group_id));
        
        // delete permissions
        $this->db->query("DELETE FROM `permission_grant_group` WHERE `permission_group_id` = :permission_group_id",array("permission_group_id"=>$group_id));
        
        // delete the group
        $this->delete($group_id);
        
        return true;
        
    }
    
    /** overwrite default safe method, to force keyNames **/
    function save($data) {
        
        // generate a keyName
        if(!empty($data['id'])) {
            if(array_key_exists('keyName',$data)) {
                if(!empty($data['keyName'])) {
                    $data['keyName'] = $this->generateKeyname($data['keyName']);
                } else {
                    if(!empty($data['title'])) {
                        $data['keyName'] = $this->generateKeyname($data['title']);
                    } else {
                        unset($data['keyName']);
                    }
                }
            }
        } else {
            if(!empty($data['keyName'])) {
                $data['keyName'] = $this->generateKeyname($data['keyName']);
            } else if(!empty($data['title'])) {
                $data['keyName'] = $this->generateKeyname($data['title']);
            } else {
                unset($data['keyName']);
            }
        }
        
        return parent::save($data);
        
    }
    
}

class PermissionGroupUser extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'permission_group_user';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "
            CREATE TABLE
                `".$this->tableName."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `permission_group_id` int(11) NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
            // default group users
            $this->userJoinGroup(1,'administrator'); // first user to admin group
            
        }
    }
    
    function getUserGroups($user_id=0) {
        
        $user_id = intval($user_id);
        if(!empty($user_id)) {
            if($user_id == 1) {
                $autoGroups = 'OR pg.`id` = 1 OR pg.`id` = 2 OR pg.`id` = 3'; // anonymous, authenticated, administrator
            } else {
                $autoGroups = 'OR pg.`id` = 1 OR pg.`id` = 2'; // anonymous and authenticated
            }
        } else {
            $autoGroups = 'OR pg.`id` = 1'; // anonymous
        }
        
        $sql = "SELECT pg.`id`,pg.`title` FROM `permission_group` as pg LEFT JOIN `permission_group_user` AS pgu ON pgu.`permission_group_id` = pg.`id` WHERE pgu.`user_id` = :user_id ".$autoGroups."  GROUP BY pg.`id`";
        return $this->db->getAll($sql,array("user_id"=>$user_id));
        
    }
    
    function setUserGroups($user_id,$group_ids) {
        
        $user_id = intval($user_id);
        if(!empty($user_id) && is_array($group_ids)) {
            
            // clear users existing groups
            $this->db->query("DELETE FROM `permission_group_user` WHERE `user_id` = :user_id",array("user_id"=>$user_id));
            
            // save new groups
            foreach($group_ids as $group_id) {
                $this->save(array("user_id"=>$user_id,"permission_group_id"=>$group_id));
            }
            
            return true;
            
        } else {
            return false;
        }
        
    }
    
    function userJoinGroup($user_id,$group_identifier) {
        
        $groupId = false;
        $user_id = intval($user_id);
        
        if(preg_match('/^[0-9]+$/',$group_identifier)) {
            $groupId = intval($group_identifier);
        } else {
            $groupId = intval($this->db->getOne("SELECT `id` FROM `permission_group` WHERE `keyName` = :identifier",array("identifier"=>$group_identifier)));
        }
        
        if($groupId) {
            return $this->save(array("user_id"=>$user_id,"permission_group_id"=>$groupId));
        } else {
            return false;
        }
        
    }
    
}

class PermissionGrantGroup extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'permission_grant_group';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE
                `".$this->tableName."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `permission_id` int(11) NOT NULL,
                    `permission_group_id` int(11) NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
            
            $permissions = new Permission;
            $allPermissions = $permissions->getAllPermissions();
            
            if(!empty($allPermissions)) {
                
                foreach($allPermissions as $permission) {
                    
                    // give default permissions to anonymous
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'page' && $permission['action'] == 'index') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'page' && $permission['action'] == 'home') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'page' && $permission['action'] == 'requiresaccount') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'login') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'register') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'forgotpassword') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'admin' && $permission['controller'] == 'page' && $permission['action'] == 'index') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'admin' && $permission['controller'] == 'login' && $permission['action'] == 'index') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'recover') {
                        $this->save(array("permission_group_id"=>1,"permission_id"=>$permission['id']));
                    }
                    
                    // give default permissions to authenticated
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'resetpassword') {
                        $this->save(array("permission_group_id"=>2,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'welcome') {
                        $this->save(array("permission_group_id"=>2,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'profile') {
                        $this->save(array("permission_group_id"=>2,"permission_id"=>$permission['id']));
                    }
                    if($permission['context'] == 'frontend' && $permission['controller'] == 'user' && $permission['action'] == 'logout') {
                        $this->save(array("permission_group_id"=>2,"permission_id"=>$permission['id']));
                    }
                    
                    // give admin all permissions
                    // dont need to give permissions to adminisrator, they always have permission
                    
                }
            }
            
        }
        
    }
    
    function getPermissions($group_id) {
        
        if($group_id == 3) { // Administrators get all permissions
            return $this->db->getAll("SELECT p.`context`, p.`controller`, p.`action` FROM `permission` as p");
        } else {
            return $this->db->getAll("SELECT p.`context`, p.`controller`, p.`action` FROM `permission` as p LEFT JOIN `permission_grant_group` as pgg ON pgg.`permission_id` = p.`id` WHERE pgg.`permission_group_id` = :group_id",array("group_id"=>$group_id));
        }
        
    }
    
    /**
     * Clears all current group permissions and remakes them
     * $permissions should be an array of arrays,
     * with permission_id and group_id keys
     */
    function setAllPermissions($groupPermissions) {
        
        if(is_array($groupPermissions)) {
            
            // clear all the previous ones
            $this->db->query("TRUNCATE ".$this->tableName);
            
            foreach($groupPermissions as $permissionGroup) {
                
                if(!empty($permissionGroup['permission_id']) && !empty($permissionGroup['group_id'])) {
                    
                    $this->save(array("permission_group_id"=>$permissionGroup['group_id'],"permission_id"=>$permissionGroup['permission_id']));
                    
                }
                
            }
        }
        
        return true;
        
    }
    
}
?>