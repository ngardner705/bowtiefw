<?php

namespace BowtieFW\Models;

class Template extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'template';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `template` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `content` longtext NOT NULL,
                `keyName` varchar(45) NOT NULL,
                `group` varchar(45) NOT NULL,
                `sortOrder` int(11) NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
    /**
     * overwrides Model->getAll
     * This one orders by sortOrder instead of id
     */
    function getAll() {
        
        $sql = "SELECT * FROM `".$this->tableName."` ORDER BY `sortOrder`";
	return $this->db->getAll($sql);
        
    }
    
}

?>