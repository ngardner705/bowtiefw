<?php

namespace BowtieFW\Models;

class Layout extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'layout';
        $this->install();
        $this->layoutAssets = new LayoutAssets();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `layout` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `description` varchar(255) NOT NULL,
                `content` longtext NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

class LayoutAssets extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'layoutAssets';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `layoutAssets` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `layout_id` int(11) NOT NULL,
                `title` varchar(45) NOT NULL,
                `type` varchar(45) NOT NULL COMMENT 'css, js',
                `asset` varchar(255) NOT NULL COMMENT 'location to asset',
                `location` varchar(45) NOT NULL COMMENT 'header or footer',
                `sortOrder` int(11) NOT NULL,
                `cssMediaType` varchar(45) DEFAULT '',
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";
            
            $this->db->query($sql);
            
            // add default assets
            $assetData = array(
                'layout_id'=>0,
                'title'=>'jQuery 1.11 Google CDN',
                'type'=>'javascript',
                'asset'=>'//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
                'location'=>'footer',
                'sortOrder'=>0
            );
            $this->save($assetData);
            $assetData = array(
                'layout_id'=>0,
                'title'=>'Bowtie Core JavaScript',
                'type'=>'javascript',
                'asset'=>'/frontend/views/js/bowtie.js',
                'location'=>'footer',
                'sortOrder'=>'1'
            );
            $this->save($assetData);
        }
        
    }
    
}


?>