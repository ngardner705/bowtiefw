<?php

namespace BowtieFW\Models;

class Route extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'route';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `route` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `url` varchar(255) NOT NULL,
                `context` varchar(45) NOT NULL,
                `controller` varchar(45) NOT NULL,
                `action` varchar(45) NOT NULL,
                `params` text NOT NULL,
                `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `mDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            
            $this->db->query($sql);
            
        }
        
    }
    
}

?>