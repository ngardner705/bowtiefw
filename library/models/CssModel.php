<?php

namespace BowtieFW\Models;

class Css extends \BowtieFW\Model {
    
    private $stylesheets = array();
    private $cssDir = 'frontend/views/css/';
    
    function __construct() {
        
        parent::__construct();
        
    }

    function loadStylesheets(){

        $temp = scandir($this->cssDir);
        if(!empty($temp)){
            foreach($temp as $t){
                $p = pathinfo($t);
                if(is_file($this->cssDir.$t) && $p['extension'] == 'css'){
                    $this->stylesheets[] = $t;
                }
            }
        }

        return $this->stylesheets;

    }

    function loadStylesheet($filename){

        if(file_exists($this->cssDir.$filename)){
            $styles = file_get_contents($this->cssDir.$filename);
            return $styles;
        }else{
            return false;
        }

    }

    function saveStylesheet($filename,$contents){

        $safeName = preg_replace('/[^a-zA-Z0-9-_\.]/','', $filename);
        $handle = fopen($this->cssDir.$safeName, 'w+');
        if($handle){
            fwrite($handle,$contents);
            fclose($handle);
            return($safeName);
        }else{
            return false;
        }

    }
    
}
?>