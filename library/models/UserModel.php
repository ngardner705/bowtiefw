<?php

namespace BowtieFW\Models;

class User extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'user';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            
            // install it
            $sql = "
            CREATE TABLE `user` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `email` varchar(255) CHARACTER SET utf8 NOT NULL,
                `password` varchar(255) CHARACTER SET utf8 NOT NULL,
                `lastLogin` datetime DEFAULT NOW(),
                `failedLogins` int(11) NOT NULL DEFAULT '0',
                `lockoutExpires` datetime NULL,
                `session_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
                `type` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
                `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `isactive` tinyint(1) NOT NULL DEFAULT '1', 
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
            $sql = "
            CREATE TABLE `userData` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user_id` int(11) NOT NULL,
                `firstname` varchar(45) NOT NULL,
                `lastname` varchar(45) NOT NULL,
                `title` varchar(255) NOT NULL,
                `company` varchar(255) NOT NULL,
                `avatar` varchar(255) NOT NULL,
                `dob` date NOT NULL,
                `phone` varchar(45) NOT NULL,
                `address` varchar(255) NOT NULL,
                `address2` varchar(255) NOT NULL,
                `city` varchar(45) NOT NULL,
                `province` varchar(45) NOT NULL,
                `zip` varchar(45) NOT NULL,
                `country` varchar(45) NOT NULL,
                `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
            ";
            $this->db->query($sql);
            
            // generate default admin login
            $this->saveUser(array("username"=>"admin","email"=>"admin@bowtiefw.com","password"=>"password"));
            
        }
        
    }
    
    /**
     * Pass in a login and password
     * Returns users id if success
     * Returns false if fails
     * Returns -1 if user is locked out
     */
    function login($login,$password) {
        
        // hash password
        $password = \BowtieFW\Security::hashPassword($password);
        
        // get the info for this email
        $sql = "SELECT `id`,`lockoutExpires`,`password` FROM `user` WHERE `username` = :login";
        $usersInfo = $this->db->getRow($sql,array('login'=>$login));
        
        if($usersInfo) {
            
            // make sure they aren't locked out
            if(strtotime($usersInfo['lockoutExpires']) < time()) {
                
                // make sure password is correct
                if($usersInfo['password'] == $password) {
                    
                    // login is valid
                    return $usersInfo['id'];
                    
                } else {
                    
                    $this->failedLogin($usersInfo['id']);
                    return false;
                    
                }
                
            } else {
                
                // locked out!
                return -1;
                
            }
            
        } else {
            
            // unknown login
            return false;
            
        }
        
    }
    
    /**
     * Records the login time
     */
    function setLastLogin($user_id) {
        
        $this->save(array('id'=>$user_id,'lastLogin'=>date("Y-m-d H:i:s"),'failedLogins'=>0));
        
    }
    
    /**
     * Gets user id of remembered email and session id
     */
    function getRememberedUserId($username,$session_id) {
        
        $user_id = $this->db->getOne("SELECT `id` FROM `user` WHERE `username` = :username AND `session_id` = :session_id",array("username"=>$username,"session_id"=>$session_id));
        return $user_id;
        
    }
    
    /**
     * Sets the session_id for a user
     */
    function setSessionId($username,$session_id) {
        
        $sql = "UPDATE `user` SET `session_id` = :session_id WHERE `username` = :username";
        $this->db->query($sql,array('username'=>$username,'session_id'=>$session_id));
        return true;
        
    }
    
    /**
     * Increases the failedLogin count and sets a lockout if
     * too many fails
     */
    function failedLogin($userId) {
        
        // get failedlogin details
        $sql = "SELECT `id`,`failedLogins` FROM `user` WHERE `id` = :id";
        $failedInfo = $this->db->getRow($sql,array('id'=>$userId));
        
        if(!empty($failedInfo)) {
            
            if($failedInfo['failedLogins'] >= 3) {
                
                // this is 4th time, lock them out!
                $sql = "UPDATE `user` SET `failedLogins` = `failedLogins`+1, `lockoutExpires` = DATE_ADD(NOW(), INTERVAL 2 HOUR) WHERE `id` = :id";
                $this->db->query($sql,array('id'=>$userId));
                
            } else {
                
                // add one to the counter
                $sql = "UPDATE `user` SET `failedLogins` = `failedLogins`+1 WHERE `id` = :id";
                $this->db->query($sql,array('id'=>$userId));
                
            }
            
        } else {
            
            // Unknown user, cant lockout unknown account
            ### ban IP?
            
        }
        
    }
    
    /**
     * Registers the users
     * Returns true on success
     * False on failure
     * -1 if Username already exists
     */
    function register($data) {
        
        if(!empty($data['username']) && !empty($data['email']) && !empty($data['password'])) {
            
            // make sure username is available
            $sql = "SELECT `id`,`username` FROM `user` WHERE `username` = :username";
            $usersInfo = $this->db->getRow($sql,array('username'=>$data['username']));
            
            if($usersInfo == false) {
                
                // hash password
                $data['password'] = \BowtieFW\Security::hashPassword($data['password']);
                
                // default avatar
                //if(empty($data['avatar'])) { $data['avatar'] = 'common/img/default-user.jpg'; }
                
                $usersId = $this->save($data);
                
                if($usersId) {
                    
                    return $usersId;
                    
                } else {
                    
                    return false;
                    
                }
                
            } else {
                
                // username exists
                return -1;
                
            }
            
        } else {
            
            return false;
            
        }
        
    }
    
    /**
     * Saves user details
     */
    function saveUser($data) {
        
        // hash password for safe storage
        if(!empty($data['password'])) { $data['password'] = \BowtieFW\Security::hashPassword($data['password']); }
        
        if($this->save($data)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    /**
     * Creates a tempCode for 1 time login
     */
    function forgottenUser($email) {
        
        // make sure this user exists
        $userInfo = $this->getUserByEmail($email);
        
        if(!empty($userInfo['id'])) {
            
            $recoverUser = new UserRecover();
            $tempCode = $recoverUser->generateRecover($email);
            
            if($tempCode) {
                
                return $tempCode;
                
            } else {
                
                return false;
                
            }
            
        } else {
            
            return false;
            
        }
        
    }
    
    /**
     * Validates then deletes 1 time login
     */
    function recoverUser($email,$code) {
        
        //make sure its valid
        $recoverUser = new UserRecover();
        $isValid = $recoverUser->useRecover($email,$code);
        return $isValid;
        
    }
    
    /**
     * Gets users details
     */
    function getUserByEmail($email) {
        
        $userInfo = $this->db->getRow("SELECT * FROM `user` WHERE `email` = :email",array("email"=>$email));
        return $userInfo;
        
    }
    
    /**
     * Deletes a user
     */
    function deleteUser($user_id) {
        
        $user_id = intval($user_id);
        
        if($user_id == 1) {
            return false; // cannot delete user 1
        } else {
            $this->delete($user_id);
            return true;
        }
        
    }
    
}

class UserData extends \BowtieFW\Model {
    
}

class UserRecover extends \BowtieFW\Model {
    
    function __construct() {
        
        parent::__construct();
        $this->tableName = 'user_recover';
        $this->install();
        
    }
    
    function install() {
        
        // check if installed
        if($this->tableExists()) {
            return true;
        } else {
            // install it
            $sql = "CREATE TABLE
                `user_recover` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `email` varchar(255) CHARACTER SET utf8 NOT NULL,
                    `tempCode` varchar(255) CHARACTER SET utf8 NOT NULL,
                    `expires` datetime NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            $this->db->query($sql);
            
        }
        
    }
    
    /**
     * Generates a new recover code for this email
     */
    function generateRecover($email) {
        
        // can only have 1 recovery record at a time
        $this->db->query("DELETE FROM `user_recover` WHERE `email` = :email",array("email"=>$email));
        
        // you have 6 hours to recover your account
        $saveData = array(
            'email'=>$email,
            'tempCode'=>\BowtieFW\Security::hashPassword(time()),
            'expires'=>date("Y-m-d H:i:s",strtotime('+6 Hour'))
        );
        
        $saved = $this->db->query("INSERT INTO `user_recover` SET `email`=:email,`tempCode`=:tempCode,`expires`=:expires",$saveData);
        
        if($saved) {
            
            return $saveData['tempCode'];
            
        } else {
            
            return false;
            
        }
        
    }
    
    /**
     * Uses recovery code.
     * If valid, returns true otherwise returns false
     * You can only use a recovery code once, then its deleted
     */
    function useRecover($email,$code) {
        
        $recoverRecord = $this->db->getRow("SELECT `id`,`expires` FROM `user_recover` WHERE `email`=:email AND `tempCode`=:code",array("email"=>$email,"code"=>$code));
        
        if(!empty($recoverRecord)) {
            
            // delete it, only good for 1 use
            $this->db->query("DELETE FROM `user_recover` WHERE `id` = :id",array("id"=>$recoverRecord['id']));
            
            // make sure its not expired
            if(strtotime($recoverRecord['expires'])>time()) {
                return true;
            } else {
                return false; // expired
            }
            
        } else {
            
            // invalud email/code
            return false;
        
        }
        
    }
    
}
?>