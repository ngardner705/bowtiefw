<?php
/**
 * Permissions
 * This is used by controllers and the view
 * to verify permission.
 *
 * Permissions are controller/action based
 */

namespace BowtieFW;

class Permission {
    
    private $permissionModel;
    private $permGroupModel;
    private $permissions;
    #$_SESSION['permissions'] // cache of permissions - set on first visit, deleted in Authentication::logout, and reset at Authentication::login
    
    function __construct() {
        
        $this->permissionModel = new Models\Permission;
        $this->setPermissions(false);
        
    }
    
    /**
     * Sets the users permissions and stores
     * them in their $_SESSION
     */
    function setPermissions($forceReload=true) {
        
        $userId = !empty($_SESSION['user_id'])?$_SESSION['user_id']:0;
        
        if(!empty($_SESSION['permissions']) && !$forceReload) {
            
            $this->permissions = $_SESSION['permissions'];
            
        } else {
            
            // reset
            $this->permissions = array();
            
            // get permissions
            $permissions = array();
            $permissions = $this->permissionModel->getPermissions($userId);
            
            foreach($permissions as $permission) {
                
                $permissionKey = $permission['context'].'_'.$permission['controller'].'_'.$permission['action'];
                $this->permissions[] = $permissionKey;
                
            }
            
            // store in session
            $_SESSION['permissions'] = $this->permissions;
            
        }
        
        return true;
        
    }
    
    function getPermissions() {
        return $this->permissions;
    }
    
    /**
     * check a permission (controller / action)
     * returns TRUE if granted, FALSE otherwise.
     * If a permission doesn't exist, by default you
     * do not have permission (returns FALSE).
     *
     * context is the interface (frontend, admin),
     * and the controller and action being requested
     */
    function checkPermission($controller,$action,$context='frontend') {
        
        $keyedPermission = $context.'_'.$controller.'_'.$action;
        if(is_array($this->permissions)) {
            if(in_array($keyedPermission,$this->permissions)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
}

?>