<?php

namespace BowtieFW;

class Authentication {
	
	private static $instance;
	private $loggedIn;
	public $user_id;
	public $messages;
	
	function __construct() {
		
		$this->messages = array();
		$this->loggedIn = false;
		
		if(!empty($_SESSION['user_id'])) {
			
			// log them in if they have session
			$this->setLogin($_SESSION['user_id']);
			
		} else if(!empty($_COOKIE['login']) && !empty($_COOKIE['session_id'])) {
			
			// log them in if they have cookies to remember them
			$this->autoLogin($_COOKIE['login'],$_COOKIE['session_id']);
			
		}
		
	}
	
	static function getInstance() {
		
		if (!isset(self::$instance)) {
			
			$c = __CLASS__;
			self::$instance = new $c;
			
		}
		
		return self::$instance;
		
	}
	
	function login($username,$password,$rememberMe=false) {
		
		// log them in
		$userModel = new \BowtieFW\Models\User;
		$user_id = $userModel->login($username,$password);
		
		if($user_id === false) {
			
			$this->messages[] = array("type"=>"error","message"=>"Invalid login and/or password");
			return false;
			
		} else if($user_id === -1) {
			
			$this->messages[] = array("type"=>"error","message"=>"Account is locked out, please try again later");
			return false;
			
		} else {
			
			if($rememberMe==true) { $this->rememberMe($username); }
			$this->setLogin($user_id);
			return true;
			
		}
		
	}
	
	function requiresAccount() {
		
		if($this->loggedIn()) {
			
			return true;
			
		} else {
			
			Controller::redirect("page/requiresaccount");
			
		}
		
	}
	
	function autoLogin($username,$session_id) {
		
		$userModel = new \BowtieFW\Models\User;
		$user_id = $userModel->getRememberedUserId($username,$session_id);
		
		if($user_id) {
			
			$this->setLogin($user_id);
			$this->rememberMe($username); // remember them again
			return true;
			
		} else {
			
			// no error for failed autologins
			return false;
			
		}
		
	}
	
	function setLogin($user_id) {
		
		$this->user_id = $user_id;
		$this->loggedIn = true;
		
		$_SESSION['user_id'] = $user_id;
		
		// set login date
		$userModel = new \BowtieFW\Models\User;
		$userModel->setLastLogin($user_id);
		
		// permissions
		$permissions = new Permission;
		$permissions->setPermissions(true);
		
	}
	
	function logout() {
		
		$this->user_id = false;
		$_SESSION['user_id'] = false;
		$_SESSION['permissions'] = false;
		unset($_SESSION['user_id']);
		unset($_SESSION['permissions']);
		
		// forget them
		setcookie('login','',time()-3600,'/',null,false,true);
		setcookie('session_id','',time()-3600,'/',null,false,true);
		
		return true;
		
	}
	
	function loggedIn() {
		
		if($this->loggedIn && !empty($this->user_id)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	function rememberMe($username) {
		
		$expires = time()+60*60*24*90; // 90 days
		$session_id = md5($username.time());
		setcookie('login',$username,$expires,'/',null,false,true);
		setcookie('session_id',$session_id,$expires,'/',null,false,true);
		
		$userModel = new \BowtieFW\Models\User;
		$userModel->setSessionId($username,$session_id);
		
		return true;
		
	}
	
}

?>